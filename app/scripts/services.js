
angular.module('Kidz').service('httpCalling', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api, data){
        return $http({
            method : method,
            url : $rootScope.host + api,
            data : data,
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
        .then(function(result){
        	// console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}]);


angular.module('Kidz').service('UserApiCalling', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api, data, access_token){
        return $http({
            method : method,
            url : $rootScope.host + api,
            data : data,
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            headers: {
            	"Authorization": "Token " + access_token,
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
        .then(function(result){
            return result;
        })
    }
    return { apiCalling: apiCalling };
}]);


angular.module('Kidz').service('UserApiCallingMPF', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api, data, access_token){
        return $http({
            method : method,
            url : $rootScope.host + api,
            data : data,
            headers: {
                "Authorization": "Token " + access_token,
                "Content-Type": "multipart/form-data"
            }
        })
        .then(function(result){
            // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}]);


angular.module('Kidz').service('UserQueryApiCalling', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api, access_token){
        return $http({
            method : method,
            url : $rootScope.host + api,
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            headers: {
                "Authorization": "Token " + access_token,
                "Content-Type": "application/json"
            }
        })
        .then(function(result){
            // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}]);


// angular.module('Kidz').factory("StaticApis",['$http', '$rootScope', function($http, $rootScope){
//     $http({
//         method : method,
//         url : "https://api.kidzplus.in/api/activity_type/get_all/",
//         transformRequest: function(obj) {
//             var str = [];
//             for(var p in obj)
//             str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
//             return str.join("&");
//         },
//         headers: {
//             "Authorization": "Token " + access_token,
//             "Content-Type": "application/json"
//         }
//     })
//     .success(function(data, status, headers, config){
//         console.log(data)
//         $rootScope.static_activities=data;
//         $rootScope.$broadcast("activities");
//         // console.log($rootScope.static_activities)
//     });

//     // $http({
//     //     method : 'GET',
//     //     url : 'https://api.kidzplus.in/api/activity_type/get_all/',
//     //     responseType : "json",
//     //     headers: {
//     //         "Content-Type": 'application/json'
//     //     }
//     // })
//     // .success(function(data, status, headers, config){
//     //     $rootScope.static_activities=data;
//     //     $rootScope.$broadcast("activities");
//     //     // console.log($rootScope.static_activities)
//     // });
//     return true
// }]);


angular.module('Kidz').service("uploadImage",['$http', '$rootScope', "Upload", function($http, $rootScope, Upload){
    var apiCalling = function(method, api, send_data, access_token){
        return Upload.upload({
            method : method,
            url : $rootScope.host + api,
            data : send_data,
            headers: {
                "Authorization": "Token " + access_token,
                "Content-Type": undefined
            }
        })
        .then(function(result){
            // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };

}]);



// angular.module('Kidz').directive('ngFiles', ['$parse', function ($parse) {

//     function fn_link(scope, element, attrs) {
//         var onChange = $parse(attrs.ngFiles);
//         element.on('change', function (event) {
//             onChange(scope, { $files: event.target.files });
//         });
//     };

//     return {
//         link: fn_link
//     }
// } ])

