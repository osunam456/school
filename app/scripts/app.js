'use strict';

angular.module('Kidz', [
  'ui.router',
  'ngSanitize',
  'ngResource',
  'ngCookies',
  'ngAnimate',
  'ui.bootstrap',
  'ngFileUpload',
  'ui.bootstrap.datetimepicker',
  'angularFileUpload'
])

  .config(['$stateProvider' , '$urlRouterProvider', '$locationProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

    $locationProvider.html5Mode(true)
    $locationProvider.hashPrefix('!');

    $urlRouterProvider.otherwise('');
    $urlRouterProvider.when('','/');

    $stateProvider
      .state('Home',{
        url : '/',
        templateUrl : 'views/schooladmin/login.html',
        controller : 'Home',
      })

      .state('SchoolAdminForgotPassword',{
        url : 'forgot_password/',
        parent : "Home",
        templateUrl : 'views/schooladmin/forgot_password.html',
        controller : 'SchoolAdminForgotPassword',
      })

      .state('SchoolAdminChangePassword',{
        url : 'change_password/:code/',
        parent : "Home",
        templateUrl : 'views/schooladmin/change_password.html',
        controller : 'SchoolAdminForgotPassword',
      })








      .state('SchoolAdminDashboard',{
        url : 'schooladmin/main/',
        parent : "Home",
        templateUrl : 'views/schooladmin/dashboard.html',
        controller : 'SchoolAdminDashboard',
      })

      .state('SchoolProfile',{
        url : 'school-profile/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/profile.html',
        controller : 'SchoolProfile',
      })










      .state('SchoolAdminClassrooms',{
        url : 'classrooms/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/classrooms/classrooms.html',
        controller : 'SchoolAdminClassrooms',
      })

      .state('SchoolAdminCreateClassroom',{
        url : 'create-classroom/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/classrooms/create-classroom.html',
        controller : 'SchoolAdminCreateClassroom',
      })

      .state('SchoolAdminEditClassroom',{
        url : 'edit-classroom/:id_classroom/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/classrooms/edit-classroom.html',
        controller : 'SchoolAdminEditClassroom',
      })






      
      .state('SchoolAdminTeachers',{
        url : 'teachers/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/teachers/teachers.html',
        controller : 'SchoolAdminTeachers',
      })
      .state('SchoolAdminCreateTeacher',{
        url : 'create-teacher/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/teachers/create-teacher.html',
        controller : 'SchoolAdminCreateTeacher',
      })
      .state('SchoolAdminEditTeacher',{
        url : 'edit-teacher/:id_teacher/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/teachers/edit-teacher.html',
        controller : 'SchoolAdminEditTeacher',
      })
      
      // .state('SchoolAdminActivities',{
      //   url : 'activities/',
      //   parent : "SchoolAdminDashboard",
      //   templateUrl : 'views/schooladmin/activities/activities.html',
      //   controller : 'SchoolAdminActivities',
      // })
      // .state('SchoolAdminCreateActivity',{
      //   url : 'create-activity/',
      //   parent : "SchoolAdminDashboard",
      //   templateUrl : 'views/schooladmin/activities/create-activity.html',
      //   controller : 'SchoolAdminCreateActivity',
      // })
      // .state('SchoolAdminEditActivity',{
      //   url : 'edit-activity/',
      //   parent : "SchoolAdminDashboard",
      //   templateUrl : 'views/schooladmin/activities/edit-activity.html',
      //   controller : 'SchoolAdminEditActivity',
      // })







      .state('SchoolAdminStudents',{
        url : 'students/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/students/students.html',
        controller : 'SchoolAdminStudents',
      })
      .state('SchoolAdminCreateStudent',{
        url : 'create-student/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/students/create-student.html',
        controller : 'SchoolAdminCreateStudent',
      })
      .state('SchoolAdminEditStudent',{
        url : 'edit-student/:id_student/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/students/edit-student.html',
        controller : 'SchoolAdminEditStudent',
      })
      


      .state('SchoolAdminEssentials',{
        url : 'essentials/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/essentials/essentials.html',
        controller : 'SchoolAdminEssentials',
      })
      .state('SchoolAdminCreateEssential',{
        url : 'create-essential/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/essentials/create-essential.html',
        controller : 'SchoolAdminCreateEssential',
      })
      .state('SchoolAdminEditEssential',{
        url : 'edit-essential/:id_essential/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/essentials/edit-essential.html',
        controller : 'SchoolAdminEditEssential',
      })
      

      

      .state('SchoolDateWiseReport',{
        url : 'date-wise-report/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/attendance_report/date_wise_report.html',
        controller : 'SchoolDateWiseReport',
      })
      .state('SchoolIndividualStudentReport',{
        url : 'individual-student-report/',
        parent : "SchoolAdminDashboard",
        templateUrl : 'views/schooladmin/attendance_report/individual_report.html',
        controller : 'SchoolIndividualStudentReport',
      })







      .state('SuperuserLogin',{
        url : 'superuser/',
        parent : "Home",
        templateUrl : 'views/superuser/login.html',
        controller : 'SuperuserLogin',
      })

      .state('SuperuserMain',{
        url : 'main/',
        parent : "SuperuserLogin",
        templateUrl : 'views/superuser/dashboard.html',
        controller : 'SuperuserMain',
      })

      .state('SuperuserBulkUpload',{
        url : 'bulk-upload/',
        parent : "SuperuserMain",
        templateUrl : 'views/superuser/bulk-upload.html',
        controller : 'SuperuserBulkUpload',
      })

      .state('SuperuserEssentials',{
        url : 'essentials/',
        parent : "SuperuserMain",
        templateUrl : 'views/superuser/essentials/essentials.html',
        controller : 'SuperuserEssentials',
      })
      .state('SuperuserEditEssential',{
        url : 'edit-essential/:id_essential/',
        parent : "SuperuserEssentials",
        templateUrl : 'views/superuser/essentials/edit-essential.html',
        controller : 'SuperuserEditEssential',
      })
      .state('SuperuserCreateEssential',{
        url : 'create-essential/',
        parent : "SuperuserEssentials",
        templateUrl : 'views/superuser/essentials/create-essential.html',
        controller : 'SuperuserCreateEssential',
      })

      .state('SuperuserSchools',{
        url : 'schools/',
        parent : "SuperuserMain",
        templateUrl : 'views/superuser/schools/schools.html',
        controller : 'SuperuserSchools',
      })
      .state('SuperuserEditSchool',{
        url : 'edit-school/:id_school/',
        parent : "SuperuserSchools",
        templateUrl : 'views/superuser/schools/edit-school.html',
        controller : 'SuperuserEditSchool',
      })
      .state('SuperuserCreateSchool',{
        url : 'create-school/',
        parent : "SuperuserSchools",
        templateUrl : 'views/superuser/schools/create-school.html',
        controller : 'SuperuserCreateSchool',
      })

      .state('SuperuserActivityTypes',{
        url : 'activities/',
        parent : "SuperuserMain",
        templateUrl : 'views/superuser/activity_type/activity_types.html',
        controller : 'SuperuserActivityTypes',
      })
      .state('SuperuserCreateActivityType',{
        url : 'create-activity-type/',
        parent : "SuperuserActivityTypes",
        templateUrl : 'views/superuser/activity_type/create-activity_type.html',
        controller : 'SuperuserCreateActivityType',
      })
      .state('SuperuserEditActivityType',{
        url : 'edit-activity-type/:id_activity_type/',
        parent : "SuperuserActivityTypes",
        templateUrl : 'views/superuser/activity_type/edit-activity_type.html',
        controller : 'SuperuserEditActivityType',
      })


  }]);




