'use strict'

angular.module('Kidz').controller('SuperuserLogin', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'httpCalling', 'UserApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, httpCalling, UserApiCalling) {

	$scope.superuser = {}
    $scope.superuser_present = false
    $scope.api_data=[]


    $scope.setting_superuser = function(api_data, type){
        // console.log(api_data)
        $scope.superuser.access_token = api_data.access_token;
        $scope.superuser.email = $scope.superuser.email;

        $scope.active_schools = api_data.school_active
        $scope.inactive_schools = api_data.school_inactive
        $scope.essentials = api_data.essentials

        $scope.user_type = type
        api_data.user_type = type
        api_data.email = $scope.superuser.email
        $window.localStorage.setItem('data', JSON.stringify(api_data));
        $scope.$broadcast(type);
    }

    $scope.verifying_superuser = function(api_data, type){
        // console.log(api_data)

        $scope.active_schools = api_data.school_active
        $scope.inactive_schools = api_data.school_inactive
        $scope.essentials = api_data.essentials

        $scope.$broadcast(type);
    }

    $scope.reset_superuser_localstorage = function () {
        $window.localStorage.setItem('data', "");
    }

    $scope.superuser_login = function(){
        var send_data = {
           "password" : $scope.superuser.password,
           "email" : $scope.superuser.email,
        }
        $scope.activate_loading()
        var myDataPromise = httpCalling.apiCalling('POST', '/api/superuser/login/', send_data);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.deactivate_loading()
            if (data.result == 1) {
                $scope.api_data = data.data
                $scope.superuser_present = true;
                console.log($scope.api_data)
                $scope.setting_superuser($scope.api_data, "superuser")

                $location.path("/superuser/main/")
            }else if (data.result == 0) {
                //Verify otp modal
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    $scope.superuser_verify = function(){
        var local_storage_data = $window.localStorage.getItem('data');
        if(local_storage_data){
            local_storage_data = JSON.parse(local_storage_data)
            $scope.superuser.email = local_storage_data.email;
            $scope.superuser.access_token = local_storage_data.access_token;
            var send_data = {
                "email" : $scope.superuser.email
            }
            var myDataPromise = UserApiCalling.apiCalling('POST', '/api/superuser/verify/', send_data, $scope.superuser.access_token);
            myDataPromise.then(function(result) {  
                var data = result.data;
                console.log(data)
                if (data.result == 1) {
                    $scope.api_data = data.data
                    $scope.superuser_present = true;

                    $scope.verifying_superuser($scope.api_data, "superuser")

                    var url = $location.url();
                    if (url=="/superuser/") {
                        $location.path("/superuser/main/");
                    }
                }else if (data.result == 2) {
                    $scope.reset_superuser_localstorage()
                    $location.path("/superuser/");                    
                }
            })
        }else{
            $location.path("/superuser/");
        }
    }






    $scope.superuser_logout = function(){
        var send_data = {
            "email" : $scope.superuser.email
        }
        // console.log(send_data)
        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/superuser/logout/', send_data, $scope.superuser.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.deactivate_loading()
            if (data.result == 1) {
                $scope.superuser_present = false;
                $scope.reset_superuser_localstorage()
                $location.path("/superuser/")
            }else if (data.result == 2) {
                $scope.reset_superuser_localstorage()
                $location.path("/superuser/")                
            }
        })
    }

    $scope.superuser_init = function(){
        $scope.superuser_verify()
    }
}]);


