'use strict'

angular.module('Kidz').controller('SuperuserBulkUpload', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', 'UserApiCalling', 'FileUploader', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling, UserApiCalling, FileUploader) {

    $scope.selected_school = false
    $scope.uploader = new FileUploader({
      "onBeforeUploadItem" : function(item){
        item.formData.push({"school_id": $scope.selected_school.id});
      },
      "url" : $rootScope.host + "/api/student/bulk_upload/",
      "alias": "csv",
      "headers" : {
        "authorization": "Token " + JSON.parse($window.localStorage.data).access_token
      },
      "onProgressItem" : function(item, progress){
        $scope.activate_loading()
      },
      "onSuccessItem" : function(item, response, status, headers){
        $scope.deactivate_loading()
        console.log(response)
        if(response.result){
          $rootScope.transactions_data = response.csv_data
          $rootScope.csv_uploaded_data = true
          $scope.change_message_to_show("New Sheet has been Uploaded", 1)
          $('#redirectBox').modal('show');
        }else if(response.result==0){
          $rootScope.errors_to_show(response.errors, 2)
          $('#redirectBox').modal('show');
        }
      }
    })

    $scope.resize = function () {
      setTimeout(function(){
        $('#dash_1').height(($(window).height() - 255));
        $(window).resize(function () {
            $('#dash_1').height(($(window).height() - 255));
        });
        $('#dash_2').height(($(window).height() - 355));
        $(window).resize(function () {
            $('#dash_2').height(($(window).height() - 355));
        });
      }, 2000)
    }

    $scope.call_schools_list = function(){
      $scope.resize()
      $scope.activate_loading()
      var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/school/get_all/', $scope.superuser.access_token);
      myDataPromise.then(function(response) {  
        var data = response.data
        $scope.deactivate_loading()
        if(data.result){
          $scope.schools_list = data.data
        }
      })
    }

    $scope.school_selected = function(idd){
      $scope.schools_list.forEach(function(school) {
        if (school.id==idd) {
          $scope.selected_school = school 
        }
      });
    }

    $scope.bulk_upload_init = function(){
      if($scope.superuser_present){
        $scope.call_schools_list()
      }else{
      }
    }

    $scope.$on('superuser', function(event, args) {
      $scope.call_schools_list()
    });

}]);


