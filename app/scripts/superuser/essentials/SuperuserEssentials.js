'use strict'

angular.module('Kidz').controller('SuperuserEssentials', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', '$timeout', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling, $timeout) {

	$scope.page_count = 1
    $rootScope.ownership_status = "all"
    $scope.search = ""
    $rootScope.array_of_essentials_superuser = []
    $rootScope.thing_to_search = ""

    $scope.$watch('array_of_essentials', function() {
        // console.log("sddss", $scope.array_of_essentials_superuser)
    }, true);

    $scope.call_essentials_list = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/essential/?q='+$rootScope.thing_to_search+'&page='+$scope.page_count+'&filter='+$rootScope.ownership_status, $scope.superuser.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.essential_count = data.count
            $scope.deactivate_loading()
            
            $scope.max_number_of_pages = parseInt(data.count/10)+1
            if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                $scope.disable_next = false
                if ($scope.page_count>1) {
                    $scope.disable_prev = false
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }
            }else if($scope.max_number_of_pages==1){
                $scope.disable_prev = true
                $scope.disable_next = true
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }else if ($scope.page_count==$scope.max_number_of_pages) {
                $scope.disable_next = true
                $scope.disable_prev = false
            }

            // $timeout(function(){
            //     //any code in here will automatically have an apply run afterwards
            //     $scope.array_of_essentials_superuser = data.results
            // });
            $rootScope.array_of_essentials_superuser = data.results
            // console.log($scope.array_of_essentials_superuser)
        })
    }

    $scope.search_function = function(){
        $rootScope.thing_to_search = $scope.search
        // console.log($rootScope.thing_to_search)
        $scope.call_essentials_list()
    }

     $scope.set_to_delete = function(id){
        $scope.essential_to_delete = id
        
    }

    $scope.getNumber = function(num) {
        return new Array(num);   
    }


    $scope.delete_essential_page = function(id){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('DELETE', '/api/essential/'+$scope.essential_to_delete+'/', $scope.superuser.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            $scope.deactivate_loading() 
            if (data.result) {
                $scope.change_message_to_show(data.message, 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
            $scope.call_essentials_list()
        })
    }

    $scope.edit_essential_page = function(id){
        // console.log(id)
        $location.path("/superuser/main/essentials/edit-essential/"+id+"/")    
    }
    
    $scope.pagination_count_update = function(count){
        // console.debug(count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope.call_essentials_list()
        }
    }

    $scope.page_count_change = function(){
        // console.debug($scope.page_count)
        $scope.call_essentials_list()
    }

    $scope.change_ownership = function(value){
        $rootScope.ownership_status=value
        $scope.call_essentials_list()
    }

    $scope.essentials_init = function(){
        if($scope.superuser_present){
            $scope.call_essentials_list()
        }else{
        }
    }

    $scope.$on('superuser', function(event, args) {
        $scope.call_essentials_list()
    });
}]);


