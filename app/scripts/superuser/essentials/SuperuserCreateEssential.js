'use strict'


angular.module('Kidz').controller('SuperuserCreateEssential', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', 'uploadImage', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling, uploadImage) {

	$scope.new_essential = {}

 //    var formdata = new FormData();
 //    $scope.getTheFiles = function ($files) {
 //        angular.forEach($files, function (value, key) {
 //            formdata.append(key, value);
 //        });
 //        $scope.new_essential.image = formdata
 //        // console.debug(formdata)
 //    };

    $scope.create_essential = function(form){
    	form.submitted = true;
        // console.debug("create_essential", $scope.new_essential.image)
        var send_data = $scope.new_essential
        if (form.$valid) {
            if (!$scope.new_essential.image) {
                $scope.change_message_to_show("Please upload an image", 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.activate_loading()
                var myDataPromise = uploadImage.apiCalling('POST', '/api/essential/', send_data, $scope.superuser.access_token);
                myDataPromise.then(function(result) { 
                    // console.log(result) 
                    var data = result.data;
                    $scope.deactivate_loading()
                    if (data.result) {
                        $location.path("/superuser/main/essentials/")
                    }else{
                        $scope.change_message_to_show(data.errors, 2)
                        $('#redirectBox').modal('show');
                    }
                })
            }
        }
    }


	$scope.create_essential_init = function(){
		if($scope.superuser_present){

		}else{
		}
    }
    $scope.$on('superuser', function(event, args) {
        
    });
}]);


