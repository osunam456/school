'use strict'


angular.module('Kidz').controller('SuperuserSchools', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', 'UserApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling, UserApiCalling) {

    $scope.page_count = 1
    $rootScope.is_active = 1
    $scope.message = {}
    $scope.search = ""
    $rootScope.thing_to_search = ""

    $scope.call_schools_list = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/school/?q='+$rootScope.thing_to_search+'&page='+$scope.page_count+'&is_active='+$rootScope.is_active+'', $scope.superuser.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $rootScope.school_number = data.count
            $scope.deactivate_loading()
            $scope.max_number_of_pages = parseInt(data.count/10)+1
            console.log($scope.max_number_of_pages)
            if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                $scope.disable_next = false
                if ($scope.page_count>1) {
                    $scope.disable_prev = false
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }
            }else if($scope.max_number_of_pages==1){
                $scope.disable_prev = true
                $scope.disable_next = true
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }else if ($scope.page_count==$scope.max_number_of_pages) {
                $scope.disable_next = true
                $scope.disable_prev = false
            }

            $rootScope.array_of_schools = data.results

            console.log($rootScope.array_of_schools)
        })
    }

    $scope.open_send_modal = function(email){
        $('#sendMessage').modal('show');
        $scope.message.email = email
    }
    $scope.open_sent_modal = function(){
        $('#messageSent').modal('show');
    }

    $scope.send_message = function(form){
        // console.log($scope.message)
        $('#sendbtn').prop('disabled', true);
        var send_data = {
            "poc_email" : $scope.message.email,
            "message" : $scope.message.message
        }
        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/superuser/send_message/', send_data, $scope.superuser.access_token);
        myDataPromise.then(function(result) { 
            $scope.deactivate_loading() 
            var data = result.data;
            // console.log(data)
            if (data.result) {
                $scope.message_to_show = "Message Sent"
            }
            $('#sendbtn').prop('disabled', false);
        })

    }
    
    $scope.search_function = function(){
        $rootScope.thing_to_search = $scope.search
        // console.log($rootScope.thing_to_search)
        $scope.call_schools_list()
    }

    $scope.edit_school_page = function(id){
        // console.log(id)
        $location.path("/superuser/main/schools/edit-school/"+id+"/")    
    }
    
    $scope.pagination_count_update = function(count){
        // console.debug(count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope.call_schools_list()
        }
    }

    $scope.page_count_change = function(number){
        // console.debug($scope.page_count)
        $scope.page_count = number
        $scope.call_schools_list()
    }

    $scope.change_is_active = function(value){
        $rootScope.is_active = value
        $scope.call_schools_list()
        // console.debug("active", $rootScope.is_active)
    }
    $scope.getNumber = function(num) {
        return new Array(num);   
    }

    $scope.schools_init = function(){
        // console.debug("schools_init", $scope.superuser_present)
        // $('#toggle-active').bootstrapToggle();
        if($scope.superuser_present){
            
            $scope.call_schools_list()
        }else{
        }
    }

    $scope.$on('superuser', function(event, args) {
        $scope.call_schools_list()
    });
}]);



