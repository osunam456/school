'use strict'


angular.module('Kidz').controller('SuperuserActivityTypes', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', '$timeout', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling, $timeout) {

    $scope.page_count = 1
    $scope.search = ""
    $rootScope.array_of_activity_types  = []
    $rootScope.thing_to_search = ""
    $rootScope.ownership_status = "all"

    $scope.call_activity_types_list = function(){
        $scope.activate_loading()
        // console.debug("call_activity_types_list", $rootScope.thing_to_search)
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/activity_type/?q='+$rootScope.thing_to_search+'&page='+$scope.page_count+'&filter='+$rootScope.ownership_status, $scope.superuser.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.activity_count = data.count
            $scope.deactivate_loading()
            // console.log($scope.page_count)
            $scope.max_number_of_pages = parseInt(data.count/10)+1
            if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                $scope.disable_next = false
                if ($scope.page_count>1) {
                    $scope.disable_prev = false
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }
            }else if($scope.max_number_of_pages==1){
                $scope.disable_prev = true
                $scope.disable_next = true
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }else if ($scope.page_count==$scope.max_number_of_pages) {
                $scope.disable_next = true
                $scope.disable_prev = false
            }

            $rootScope.array_of_activity_types = data.results
        })
    }

    $scope.search_function = function(){
        $rootScope.thing_to_search = $scope.search
        // console.log($rootScope.thing_to_search)
        $scope.call_activity_types_list()
    }

    $scope.edit_activity_page = function(id){
        // console.log(id)
        $location.path("/superuser/main/activities/edit-activity-type/"+id+"/")    
    }


    $scope.set_to_delete = function(id){
        $scope.feed_to_delete = id
    }

    $scope.getNumber = function(num) {
        return new Array(num);   
    }


    $scope.delete_feed = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('DELETE', '/api/activity/'+$scope.feed_to_delete+'/', $scope.superuser.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading() 
            if (data.result) {
                $scope.change_message_to_show(data.message, 1)
                $scope.$broadcast("delete_feed");
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
            $scope.call_activity_types_list()
        })
    }

    $scope.pagination_count_update = function(count){
        // console.debug(count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope.call_activity_types_list()
                
        }
    }
    $scope.page_count_change = function(number){
        // console.debug($scope.page_count)
         $scope.page_count = number
        $scope.call_activity_types_list()
    }

    $scope.change_ownership = function(value){
        $rootScope.ownership_status=value
        $scope.call_activity_types_list()
    }

    $scope.activity_init = function(){
        if($scope.superuser_present){
            $scope.call_activity_types_list()
        }else{
        }
    }

    $scope.$on('superuser', function(event, args) {
        $scope.call_activity_types_list()
    });
    
}]);



