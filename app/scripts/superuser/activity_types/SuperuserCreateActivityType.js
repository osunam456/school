'use strict'

angular.module('Kidz').controller('SuperuserCreateActivityType', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', 'uploadImage', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling, uploadImage) {

	$scope.new_activity_type = {}

    $scope.create_activity_type = function(form){
    	form.submitted = true;
        if (!$scope.new_activity_type.image) {
            $scope.change_message_to_show("Please upload an image", 1)
            $('#redirectBox').modal('show');
        }else{
            if (form.$valid) {
                $scope.activate_loading()
            	var send_data = $scope.new_activity_type
                var myDataPromise = uploadImage.apiCalling('POST', '/api/activity_type/', send_data, $scope.superuser.access_token);
                myDataPromise.then(function(result) { 
                	// console.log(result) 
                    var data = result.data;
                    $scope.deactivate_loading()
                    if (data.result) {
                        $location.path("/superuser/main/activities/")
                    }else{
                        $scope.change_message_to_show(data.errors, 2)
                        $('#redirectBox').modal('show');
                    }
                })
            }
        }
    }

	$scope.create_activity_type_init = function(){
		if($scope.superuser_present){

		}else{
		}
    }

    $scope.$on('superuser', function(event, args) {

    });
}]);


