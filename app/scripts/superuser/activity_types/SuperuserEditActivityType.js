'use strict'


angular.module('Kidz').controller('SuperuserEditActivityType', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', 'uploadImage', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling, uploadImage) {

	$scope.particular_activity_type = {}
    $scope.image_change = 0
    $scope.edit_state = 0

    $scope.$watch('particular_image', function() {
        // console.debug('particular_image')
        if ($scope.particular_image==null) {
            $scope.image_change = 0
        }else{
            $scope.particular_activity_type.image = $scope.particular_image
            $scope.image_change = 1
        }
    }, true);

    $scope.update_activity_type = function(form){
        // console.log(form)
        // console.log($scope.particular_activity_type)
        var sent_data = $scope.particular_activity_type
        if ($scope.image_change==0) {
            delete $scope.particular_activity_type.image
        }
        $scope.activate_loading()
        var myDataPromise = uploadImage.apiCalling('POST', '/api/activity_type/'+$stateParams.id_activity_type+'/update_details/', sent_data, $scope.superuser.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            // console.log(data)
            if (data.result) {
                $scope.particular_activity_type = data.data
                $scope.particular_image = $scope.particular_activity_type.image
                $location.path("/superuser/main/activities/")
                $scope.change_message_to_show("Activity Type has been successfully edited", 1)
                $('#redirectBox').modal('show');
                $scope.message_to_show.push()
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }                              
        })
    }

    $scope.get_activity_typedata = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/activity_type/'+$stateParams.id_activity_type+'/', $scope.superuser.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.deactivate_loading()
            if (data.result) {
                $scope.particular_activity_type = data.data
                $scope.particular_image = $scope.particular_activity_type.image
            }                        
        })
    }
    $scope.trigger_edit = function () {
        $scope.edit_state = 1
    }
    $scope.cancel_edit = function () {
        $scope.edit_state = 0
    }
    // date picker
    $scope.open1 = function() {
        // console.debug("open1")
        $scope.popup1.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        // minDate: new Date(),
        // startingDay: 1  
    };
    $scope.format = "yyyy-MM-dd";
    $scope.popup1 = {
        opened: false
    };
    
	$scope.edit_activity_type_init = function(){
        // console.debug("edit_activity_type_init")
		if($scope.superuser_present){
            $scope.get_activity_typedata()
		}else{
		}
    }

    $scope.$on('superuser', function(event, args) {
        $scope.get_activity_typedata()
    });

}]);


