'use strict'

angular.module('Kidz').controller('Home', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'httpCalling', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, httpCalling, UserApiCalling, UserQueryApiCalling) {

	$rootScope.host = "https://api.kidzplus.in" 

    $scope.change_message_to_show = function(message, type){
        $rootScope.message_to_show = []
        if (type==1) {
            $rootScope.message_to_show.push(message)
        }else if(type==2){
            $rootScope.message_to_show=message
        }
        setTimeout(function(){
            $rootScope.message_to_show = []
        }, 2000);
    }

    $scope.activate_loading = function(){
        $('.loading_div').fadeIn(500);
    }

    $scope.deactivate_loading = function(){
        $('.loading_div').fadeOut(500);
    }

    
    $scope.essential_count = ""
    $scope.student_count = ""
    $scope.staff_count = ""
    $scope.class_count = ""
    $scope.activity_count = ""
    $scope.school_name = ""

    $scope.schooladmin = {}
    $scope.schooladmin_present = false

    $scope.static_activities = []
    $scope.learning_activities = []

    $scope.call_activities_list_get_all = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/activity_type/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            data.forEach(function(activity) {
                if (activity.is_learning==true) {
                    $scope.learning_activities.push(activity)
                }else{
                    $scope.static_activities.push(activity)
                }
            });
            console.debug("activate_tags", $scope.static_activities)
            console.debug("activate_tags", $scope.learning_activities)
            $scope.$broadcast("activities");
        })
    }

    


































    $scope.setting_schooladmin = function(api_data, type){
        // console.log(api_data)
        $scope.schooladmin.access_token = api_data.access_token;
        $scope.schooladmin.email = $scope.schooladmin.email;

        $scope.essential_count = api_data.essential_count
        $scope.student_count = api_data.student_count
        $scope.staff_count = api_data.staff_count
        $scope.class_count = api_data.class_count
        $scope.activity_count = api_data.activity_count
        $scope.school_name = api_data.school_name

        $scope.user_type = type
        api_data.user_type = type
        api_data.email = $scope.schooladmin.email
        $window.localStorage.setItem('data', JSON.stringify(api_data));
        $scope.$broadcast(type);
        $scope.call_activities_list_get_all()        
    }

    $scope.verifying_schooladmin = function(api_data, type){
        var local_storage_data = $window.localStorage.getItem('data');
        local_storage_data = JSON.parse(local_storage_data)
        local_storage_data.essential_count = api_data.essential_count
        local_storage_data.student_count = api_data.student_count
        local_storage_data.staff_count = api_data.staff_count
        local_storage_data.class_count = api_data.class_count
        local_storage_data.activity_count = api_data.activity_count

        $scope.essential_count = api_data.essential_count
        $scope.student_count = api_data.student_count
        $scope.staff_count = api_data.staff_count
        $scope.class_count = api_data.class_count
        $scope.activity_count = api_data.activity_count
        $scope.school_name = api_data.school_name
        
        $window.localStorage.setItem('data', JSON.stringify(local_storage_data));
        $scope.$broadcast(type);
        $scope.call_activities_list_get_all()
    }

    $scope.reset_schooladmin_localstorage = function () {
        $window.localStorage.setItem('data', "");
    }

    $scope.schooladmin_login = function(){
        var send_data = {
           "password" : $scope.schooladmin.password,
           "email" : $scope.schooladmin.email,
        }
        $scope.activate_loading()
        var myDataPromise = httpCalling.apiCalling('POST', '/api/school_admin/login/', send_data);
        myDataPromise.then(function(result) {  
            var data = result.data;
            console.log(result)
            // $scope.deactivate_loading()
            if (data.result == 1) {
                var api_data = data.data
                $scope.schooladmin_present = true;
                $scope.setting_schooladmin(api_data, "schooladmin")
                $location.path("/schooladmin/main/");
            }else if (data.result == 2) {
                var api_data = data.data
                $scope.schooladmin_present = true;
                $scope.setting_schooladmin(api_data, "schooladmin")
                $location.path("/schooladmin/main/");
            }else{
                $scope.reset_schooladmin_localstorage()
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
                $scope.deactivate_loading()
            }
        })
    }

    $scope.schooladmin_verify = function(){
        var local_storage_data = $window.localStorage.getItem('data');
        var url = $location.url();
        if(local_storage_data){
            local_storage_data = JSON.parse(local_storage_data)
            if (local_storage_data.user_type=="schooladmin") {
                $scope.schooladmin.email = local_storage_data.email;
                $scope.schooladmin.access_token = local_storage_data.access_token;
                var send_data = {
                    "email" : $scope.schooladmin.email
                }
                var myDataPromise = UserApiCalling.apiCalling('POST', '/api/school_admin/verify/', send_data, $scope.schooladmin.access_token);
                myDataPromise.then(function(result) {  
                    var data = result.data;
                    if (data.result == 1) {
                        var api_data = data.data
                        $scope.schooladmin_present = true;
                        // $scope.hideShow_error(data.message)
                        $scope.verifying_schooladmin(api_data, "schooladmin")

                        if (url=="/") {
                            $location.path("/schooladmin/main/");
                        }
                    }else if (data.result == 2) {
                        var api_data = data.data
                        $scope.schooladmin_present = true;
                        $scope.verifying_schooladmin(api_data, "schooladmin")

                        if (url=="/") {
                            $location.path("/schooladmin/main/");
                        }
                        $scope.change_message_to_show(data.errors, 2)
                        $('#redirectBox').modal('show');
                    }else{
                        $scope.reset_schooladmin_localstorage()
                        if (url=="/change_password/*" || url=="/forgot_password/") {
                        }else{
                            $location.path("/"); 
                        }
                    }
                })
            }
        }else{
            console.log($state.current.controller)
            if ($state.current.controller=="SchoolAdminForgotPassword") {
            }else{
                $location.path("/"); 
            }
        }
    }






    $scope.schooladmin_logout = function(){
        var send_data = {
            "email" : $scope.schooladmin.email
        }
        // console.log(send_data)
        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/school_admin/logout/', send_data, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            if (data.result == 1) {
                $scope.schooladmin_present = false;
                $scope.reset_schooladmin_localstorage()
                $location.path("/")
            }else if (data.result == 2) {
                $scope.reset_schooladmin_localstorage()
                $location.path("/")    
            }
        })
    }

    $scope.schooladmin_init = function(){
        $scope.schooladmin_verify()
    }

}]);


