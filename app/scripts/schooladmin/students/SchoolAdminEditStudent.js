'use strict'



angular.module('Kidz').controller('SchoolAdminEditStudent', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', 'UserApiCalling', 'uploadImage', '$filter', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling, UserApiCallin, uploadImage, $filter) {

    $scope.particular_student = {}
    $scope.dob_date =  new Date();
    $scope.image_change = 0
    $scope.particular_image = null
    $scope.edit_state = 0
    $scope.all_classes = []
    $scope.classes_selected = []

    $scope.$watch('particular_image', function() {
        if ($scope.particular_image==null) {
            $scope.image_change = 0
        }else{
            $scope.particular_student.profile_image = $scope.particular_image
            $scope.image_change = 1
        }
    }, true);

    $scope.dob_change = function(){
        $scope.particular_student.dob=$filter('date')($scope.dob_date,'dd/MM/yyyy');
    }

    $scope.update_student = function(){
        if ($scope.image_change==0) {
            delete $scope.particular_student.profile_image
        }
        if ($scope.particular_student.classes_selected_form) {
            var selected_classrooms = $scope.particular_student.classes_selected_form.split(',')
            var selected_classrooms_ids = []
            $scope.all_classes.forEach(function(particular_class) {
                for (var i = 0; i < selected_classrooms.length; i++) {
                    if (particular_class.name==selected_classrooms[i]) {
                        selected_classrooms_ids.push(particular_class.id) 
                    }
                }
            });
            var selected_classrooms_ids_strings = selected_classrooms_ids.join(",");
            console.log(selected_classrooms_ids_strings)
            $scope.particular_student.class_list = selected_classrooms_ids_strings
        }
        var send_data = $scope.particular_student
        $scope.activate_loading()
        var myDataPromise = uploadImage.apiCalling('POST', '/api/student/'+$stateParams.id_student+'/update_details/', send_data, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            $scope.deactivate_loading()
            if (data.result) {
                $scope.particular_student = data.data
                $scope.change_message_to_show("Student has been successfully edited", 1)
                $('#redirectBox').modal('show');
                $("#myClasses").tagit("removeAll");

                $scope.classes_selected = []
                var class_array = $scope.particular_student.class_list.split(',')

                class_array.forEach(function(class_id){
                    $scope.all_classes.forEach(function(classroom){
                        if (classroom.id==class_id) {
                            $scope.classes_selected.push(classroom)
                        }
                    })
                })

                $scope.dob_date = new Date($scope.particular_student.dob)
                $scope.edit_state = 0
                $location.path("/schooladmin/main/students/");

                
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }                        
        })
    }


    $scope.get_studentdata = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/student/'+ $stateParams.id_student +'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            console.log("get_studentdata", data)
            $scope.deactivate_loading()
            if (data.result) {
                $scope.particular_student = data.data
                $scope.dob_date = new Date($scope.particular_student.dob);
                $scope.particular_image = $scope.particular_student.profile_image
                console.log($scope.particular_image)
                var class_array = $scope.particular_student.class_list.split(',')
                class_array.forEach(function(class_id){
                    $scope.all_classes.forEach(function(classroom){
                        if (classroom.id==class_id) {
                            $scope.classes_selected.push(classroom)
                        }
                    })
                })
            }                        
        })
    }
    $scope.get_all_classes = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/class/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.all_classes = data  
            $scope.get_studentdata()   
        })
    }
    $scope.activate_all_tags = function(){
        setTimeout(function(){ 
            var class_names = []
            $scope.all_classes.forEach(function(particular_class){
                class_names.push(particular_class.name)
            })
            $("#myClasses").tagit({
                availableTags: class_names,
                autocomplete: {delay: 0, minLength: 0},
                showAutocompleteOnFocus : true
            });
            console.log($scope.classes_selected)
            $scope.classes_selected.forEach(function(activity){
                $("#myClasses").tagit("createTag", activity.name);
            })
        }, 500)
    }












    $scope.open1 = function() {
        $scope.popup1.opened = true;
    }
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22)
    }
    $scope.format = "dd-MM-yyyy";
    $scope.popup1 = {
        opened: false
    }

    $scope.trigger_edit = function(){
        $scope.edit_state = 1
        $scope.activate_all_tags()

    }
    $scope.cancel_edit = function(){
        $scope.edit_state = 0
    }

    $scope.edit_student_init = function(){
        if($scope.schooladmin_present){
            $scope.get_all_classes()
        }else{
        }
    }

    $scope.$on('schooladmin', function(event, args) {
        $scope.get_all_classes()
    })
}])


