'use strict'


angular.module('Kidz').controller('SchoolAdminCreateStudent', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', 'UserApiCalling', 'uploadImage', '$filter', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling, UserApiCalling, uploadImage, $filter) {

	$scope.new_student = {}
    $scope.dob_date =  new Date();

    $scope.dob_change = function(){
        // console.debug("dob_change")
        // console.log($scope.dob_date)
        $scope.new_student.dob=$filter('date')($scope.dob_date, 'yyyy-MM-dd');
        // console.log($scope.new_student)
    }

	$scope.get_all_classes = function(){
        setTimeout(function(){ 
            var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/class/get_all/', $scope.schooladmin.access_token);
            myDataPromise.then(function(result) {  
                var data = result.data;
                // console.log(data)
                $scope.all_classes = data


                var class_names = []
                $scope.all_classes.forEach(function(particular_class){
                    class_names.push(particular_class.name)
                })

                $("#myClasses").tagit({
                    availableTags: class_names,
                    autocomplete: {delay: 0, minLength: 0},
                    showAutocompleteOnFocus : true
                });
                            
            })
        }, 1000)
    }

    $scope.create_student = function(form){
    	form.submitted = true;
        if (form.$valid) {
            if (!$scope.particular_image) {
                $scope.change_message_to_show("Please upload an image", 1)
                $('#redirectBox').modal('show');
                $location.path("/schooladmin/main/students/")
                
            }else{
                $scope.activate_loading()
                console.log($scope.classes_selected)
                var selected_classrooms = $scope.classes_selected.split(',')
                var selected_classrooms_ids = []
                $scope.all_classes.forEach(function(particular_class) {
                    for (var i = 0; i < selected_classrooms.length; i++) {
                        if (particular_class.name==selected_classrooms[i]) {
                            selected_classrooms_ids.push(particular_class.id) 
                        }
                    }
                });
                var selected_classrooms_ids_strings = selected_classrooms_ids.join(",");
                $scope.new_student.class_list = selected_classrooms_ids_strings

                var send_data = $scope.new_student

                var myDataPromise = uploadImage.apiCalling('POST', '/api/student/', send_data, $scope.schooladmin.access_token);
                myDataPromise.then(function(result) { 
                    // console.log(result) 
                    var data = result.data;
                    $scope.deactivate_loading()
                    if (data.result) {
                       $location.path("/schooladmin/main/students/")
                    }else{
                        $scope.change_message_to_show(data.errors, 2)
                        $('#redirectBox').modal('show');
                    }
                })
            }
        }
    }


    $scope.open1 = function() {
        // console.debug("open1")
        $scope.popup1.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22)
        // minDate: new Date(),
        // startingDay: 1  
    };
    $scope.format = "yyyy-MM-dd";
    $scope.popup1 = {
        opened: false
    };


	$scope.create_students_init = function(){
		if($scope.schooladmin_present){
			$scope.get_all_classes()
            $scope.dob_change()
		}else{

		}
    }


    $scope.$on('schooladmin', function(event, args) {
    	$scope.get_all_classes()
        $scope.dob_change()
    });





}]);


