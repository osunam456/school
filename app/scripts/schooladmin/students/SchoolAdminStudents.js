'use strict'

angular.module('Kidz').controller('SchoolAdminStudents', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling) {

	$scope.page_count = 1
    $scope.thing_to_search = ""
    $scope.search = ""
    $scope.view = false

    
    $scope.call_students_list = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/student/?q='+$scope.thing_to_search+'&page='+$scope.page_count, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.deactivate_loading()

            $scope.max_number_of_pages = parseInt(data.count/10)+1
            if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                $scope.disable_next = false
                if ($scope.page_count>1) {
                    $scope.disable_prev = false
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }
            }else if($scope.max_number_of_pages==1){
                $scope.disable_prev = true
                $scope.disable_next = true
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }else if ($scope.page_count==$scope.max_number_of_pages) {
                $scope.disable_next = true
                $scope.disable_prev = false
            }

            $rootScope.array_of_students = data.results
            $scope.total_student_count = data.count
        })
    }

    $scope.getNumber = function(num) {
        return new Array(num);   
    }

    $scope.search_function = function(){
        $scope.thing_to_search = $scope.search
        $scope.call_students_list()
    }

    $scope.edit_students_page = function(id){
        $location.path("/schooladmin/main/edit-student/"+id+"/")    
    }

    $scope.set_to_delete = function(id){
        $scope.student_to_delete = id
    }

    $scope.delete_student_page = function(){
        console.log($scope.student_to_delete)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('DELETE', '/api/student/'+$scope.student_to_delete+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            $scope.deactivate_loading() 
            if (data.result) {
                $scope.change_message_to_show(data.message, 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
            $scope.call_students_list()
        })
    }

    $scope.pagination_count_update = function(count, segment_function){
        // console.debug(count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope[segment_function]()
        }
    }
    
    $scope.page_count_change = function(number, segment_function){
    	// console.debug($scope.page_count)
        $scope.page_count = number
    	$scope[segment_function]()
    }

    $scope.change_ownership = function(value){
        $scope.ownership_status=value
        $scope.call_students_list()
    }

	$scope.students_init = function(){
		if($scope.schooladmin_present){
			$scope.call_students_list()
		}else{

		}
    }

    $scope.$on('schooladmin', function(event, args) {
		$scope.call_students_list()
    });
}]);


