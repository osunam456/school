'use strict'

angular.module('Kidz').controller('SchoolAdminActivities', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', 'UserApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling, UserApiCalling) {

	$scope.page_count = 1
    $scope.all_pending_status = "All"
    $scope.filter_teacher_id = ""
    $scope.filter_class_id = ""

    $scope.parent_staff_status = ""
    $scope.classroom_select = 'Select Class'
    $scope.teacher_select = 'Select Teacher'
    $scope.feed_for_approval_id = null
    $rootScope.particular_feed = {}

    $scope.openCalendar = function(e, picker) {
        $scope[picker].open = true;
    };

    $scope.picker3 = {
        date: new Date()
    };
    console.log($scope.picker3.date)

    $scope.call_classes_list = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/class/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.array_of_classes = data
        })
    }
    $scope.call_teachers_list = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/staff/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.array_of_teachers = data
        })
    }
    $scope.call_activities_list = function(){
        // console.debug("call_activities_list")
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/activity/?page='+$scope.page_count+'&filter_pending='+$scope.all_pending_status+'&filter_type='+$scope.parent_staff_status+'&filter_class_id='+$scope.filter_class_id+'&filter_teacher_id='+$scope.filter_teacher_id+'&q=', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()

            $scope.max_number_of_pages = parseInt(data.count/10)+1
            if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                $scope.disable_next = false
                if ($scope.page_count>1) {
                    $scope.disable_prev = false
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }
            }else if($scope.max_number_of_pages==1){
                $scope.disable_prev = true
                $scope.disable_next = true
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }else if ($scope.page_count==$scope.max_number_of_pages) {
                $scope.disable_next = true
                $scope.disable_prev = false
            }
            // console.log(data)
            $scope.array_of_feeds = data.results
        })
    }

    $scope.all_pending_list = function(status){
        $scope.all_pending_status = status
        $scope.call_activities_list()
    }

    $scope.parent_staff = function(status){
        $scope.parent_staff_status = status
        $scope.call_activities_list()
    }

    $scope.classroom_change = function(idd, name){
        $scope.classroom_select = name
        $scope.filter_class_id = idd
        // console.debug($scope.filter_class_id)
        $scope.call_activities_list()
    }

    $scope.teacher_change = function(idd, name){
        $scope.teacher_select = name
        $scope.filter_teacher_id = idd
        $scope.call_activities_list()
    }

    $scope.set_for_approval = function(idd){
        $scope.feed_for_approval_id = idd
    }

    $scope.set_feed = function(idd){
        $scope.array_of_feeds.forEach(function(feed){
            if (feed.id==idd) {
                $rootScope.particular_feed = feed
                console.log($rootScope.particular_feed)
                $rootScope.particular_feed.time2_edit_local = new Date ($rootScope.particular_feed.time2)
                
                $rootScope.particular_feed.time2_edit = new Date ($rootScope.particular_feed.time2).getTimezoneOffset()

                $rootScope.particular_feed.time_to_show = new Date( $rootScope.particular_feed.time2_edit_local.getTime() + ($rootScope.particular_feed.time2_edit * 60000));

                // $scope.particular_feed.time_to_be_viewed = $rootScope.particular_feed.time_to_show 
                // $rootScope.particular_feed.time_to_be_viewed.setHours($rootScope.particular_feed.time_to_show.getHours() + 5);
                // $rootScope.particular_feed.time_to_be_viewed.setMinutes($rootScope.particular_feed.time_to_show.getMinutes() + 30);

                console.debug($rootScope.particular_feed.time_to_show, $rootScope.particular_feed.time2_edit_local, $rootScope.particular_feed.time2_edit)
            }
        })
    }

    $scope.edit_feed = function(){
        console.log($rootScope.particular_feed)
        // $rootScope.particular_feed.time2_edit.setHours($rootScope.particular_feed.time2_edit.getHours() - 11);
        // $rootScope.particular_feed.time2_edit.setMinutes($rootScope.particular_feed.time2_edit.getMinutes() + 30);
        var send_data = {
            "text":$rootScope.particular_feed.item_content,
            "time":$rootScope.particular_feed.time_to_show
        }
        console.log(send_data)
        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/activity/'+$rootScope.particular_feed.id+'/update_details/', send_data, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            // console.log(data)
            if (data.result) {
                $scope.change_message_to_show("Feed has been successfully edited", 1)
                $('#redirectBox').modal('show');
                $scope.call_activities_list() 
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    $scope.approve_activity = function(value){
        if (value==1) {
            var myDataPromise = UserQueryApiCalling.apiCalling('POST', '/api/activity/'+$scope.feed_for_approval_id+'/approve/', $scope.schooladmin.access_token);
            myDataPromise.then(function(result) {  
                var data = result.data;
                // console.log(data)
                if (data.result) {
                    $scope.call_activities_list() 
                }else{
                    
                }
                $scope.array_of_feeds = data.results
            })
        }
    }

    $scope.pagination_count_update = function(count){
        // console.debug(count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope.call_activities_list()
        }
    }
    $scope.page_count_change = function(){
    	// console.debug($scope.page_count)
    	$scope.call_classes_list()
        $scope.call_teachers_list()
        $scope.call_activities_list()
    }

	$scope.feed_init = function(){
		if($scope.schooladmin_present){
			$scope.call_classes_list()
            $scope.call_teachers_list()
            $scope.call_activities_list()
		}else{

		}
    }

    $scope.set_to_delete = function(id){
        $scope.feed_to_delete = id
    }
    $scope.delete_feed = function(){
        console.log($scope.feed_to_delete)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('DELETE', '/api/activity/'+$scope.feed_to_delete+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            $scope.deactivate_loading() 
            if (data.result) {
                $scope.change_message_to_show(data.message, 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
            $scope.call_activities_list()
        })
    }

    $scope.$on('schooladmin', function(event, args) {
		$scope.call_classes_list()
        $scope.call_teachers_list()
        $scope.call_activities_list()
    });
}]);


