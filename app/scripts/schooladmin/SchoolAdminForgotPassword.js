'use strict'

angular.module('Kidz').controller('SchoolAdminForgotPassword', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'httpCalling', 'UserQueryApiCalling', '$filter', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, httpCalling, UserQueryApiCalling, $filter) {
    
    $scope.link_valid = true
    $scope.send_verification_email = function(){
        var send_data = {
            "email" : $scope.email_verification
        }
        console.log(send_data)
        var myDataPromise = httpCalling.apiCalling('POST', '/api/school_admin/forgot_password/', send_data);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            if (data.result) {
                $scope.change_message_to_show("A Password-Reset Email has been sent to you. Please click on the link sent to your mail to change your password.", 1)
                $('#redirectBox').modal('show');
                $('#myForm')[0].reset();
                
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }     
        })
    }

    $scope.change_password = function(){
        if ($scope.new_password==$scope.confirm_password) {
            var send_data = {
                "code" : $scope.code,
                'password' : $scope.new_password
            }
            // console.log(send_data)
            var myDataPromise = httpCalling.apiCalling('POST', '/api/school_admin/reset_password/', send_data);
            myDataPromise.then(function(result) {  
                // console.log(result)
                var data = result.data;
                if (data.result) {
                    $scope.change_message_to_show("Your Password has been changed.", 1)
                    $('#redirectBox').modal('show');
                    $('#myForm')[0].reset();
                }else{
                    $scope.change_message_to_show(data.errors, 2)
                    $('#redirectBox').modal('show');
                }     
            })
        }
    }

    $scope.check_code = function(){
        var send_data = {
            "code" : $scope.code,
        }
        // console.log(send_data)
        var myDataPromise = httpCalling.apiCalling('POST', '/api/school_admin/code_check/', send_data);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            if (data.result) {
                $scope.link_valid = true;
            }else{
                $scope.link_valid = false
            }     
        })
    }

    $scope.change_password_init = function(){
        $scope.code = $stateParams.code
        $scope.check_code()
    }
}]);


