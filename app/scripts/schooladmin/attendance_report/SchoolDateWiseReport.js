'use strict'

angular.module('Kidz').controller('SchoolDateWiseReport', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'httpCalling', 'UserQueryApiCalling', '$filter', 'UserApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, httpCalling, UserQueryApiCalling, $filter, UserApiCalling) {

    $scope.current_date = new Date();
    $scope.from_date =  new Date();
    $scope.to_date =  new Date();
    $scope.day_file_got = false;
    $scope.month_file_got = false;
    $scope.report_type = null
    $scope.month = null
    $scope.month_name = null

    $scope.dtmax = new Date();
    console.log($scope.dtmax)
    $scope.options = {
        maxDate : $scope.dtmax,
    };

    $scope.get_day_report = function(){
        console.log("get_day_report")
        var url;
        if ($scope.report_type) {
            if ($scope.report_type == "teacher") {
                url = "/api/school_admin/teacher_report/"
            }else{
                url = "/api/school_admin/student_report/"
            }
            var send_data = {
                "date": $scope.from_date_to_send
            }
            console.log(send_data)
            $scope.activate_loading()
            var myDataPromise = UserApiCalling.apiCalling('POST', url, send_data, $scope.schooladmin.access_token);
            myDataPromise.then(function(result) {  
                // console.log(result)
                var data = result.data;
                $scope.deactivate_loading()
                console.log(data)
                if (data.result) {
                    $scope.day_file_got = true
                    $scope.day_file_url = data.data.url
                    console.log(data.data.url)
                }else{
                    $scope.day_file_got = false
                    $scope.change_message_to_show(data.errors, 2)
                    $('#redirectBox').modal('show');
                }     
            })
        }else{
            $scope.change_message_to_show(['Please select Report Type'], 2)
            $('#redirectBox').modal('show');
        }
    }

    $scope.from_date_change = function(){
        $scope.from_date_to_send=$filter('date')($scope.from_date, 'dd-MM-yyyy')
        $scope.year = parseInt($filter('date')($scope.to_date, 'yyyy'))
        $scope.day_file_got = false;
        if ($scope.report_type && $scope.from_date!=$scope.current_date) {
            $scope.get_day_report()
        }
        
    }






















    $scope.get_month_report = function(){
        console.log($scope.to_date_to_send)
        $scope.month_start_date = new Date($scope.year, $scope.month -1 , 1)
        $scope.month_start_date = $filter('date')($scope.month_start_date, 'dd-MM-yyyy')
        console.log($scope.month_start_date)
        $scope.month_end_date = new Date($scope.year, $scope.month , 0)
        $scope.month_end_date = $filter('date')($scope.month_end_date, 'dd-MM-yyyy')
        if ($scope.report_type) {
            var send_data = {
                "start_date": $scope.month_start_date,
                "end_date": $scope.month_end_date,
                "user": $scope.report_type
            }
                
            console.log(send_data)
            $scope.activate_loading()
            var myDataPromise = UserApiCalling.apiCalling('POST', '/api/school_admin/attendance_report/', send_data, $scope.schooladmin.access_token);
            myDataPromise.then(function(result) {  
                // console.log(result)
                var data = result.data;
                $scope.deactivate_loading()
                console.log(data)
                if (data.result) {
                    $scope.month_file_got = true
                    $scope.month_file_url = data.data.url
                    console.log(data.data.url)
                }else{
                    $scope.month_file_got = false
                    $scope.change_message_to_show(data.errors, 2)
                    $('#redirectBox').modal('show');
                }     
            })
        }else{
            $scope.change_message_to_show(['Please select Report Type'], 2)
            $('#redirectBox').modal('show');
        }
    }





    




    $scope.to_date_change = function(){
        $scope.to_date_to_send=$filter('date')($scope.to_date, 'dd-MM-yyyy')
        $scope.month = parseInt($filter('date')($scope.to_date, 'MM'))
        $scope.day_file_got = false;
        $scope.month_file_got = false;

        switch($scope.month){
            case 1:
                $scope.month_name = 'January'
                break;
            case 2:
                $scope.month_name = 'February'
                break;
            case 3:
                $scope.month_name = 'March'
                break;
            case 4:
                $scope.month_name = 'April'
                break;
            case 5:
                $scope.month_name = 'May'
                break;
            case 6:
                $scope.month_name = 'June'
                break;
            case 7:
                $scope.month_name = 'July'
                break;
            case 8:
                $scope.month_name = 'August'
                break;
            case 9:
                $scope.month_name = 'September'
                break;
            case 10:
                $scope.month_name = 'October'
                break;
            case 11:
                $scope.month_name = 'November'
                break;
            case 12:
                $scope.month_name = 'December'
                break;
        }
        $scope.year = parseInt($filter('date')($scope.to_date, 'yyyy'))

        if ($scope.report_type && $scope.to_date!=$scope.current_date) {
            $scope.get_month_report()
        }
    }











    $scope.select_report_segment = function(category, segment){
        $scope.report_type = segment
        if (category=="daily") {
            $scope.get_day_report()
        }else if (category=="monthly") {
            $scope.get_month_report()
        }
    }




    $scope.open1 = function() {
        $scope.popup1.opened = true;
    }
    $scope.open2 = function() {
        $scope.popup2.opened = true;
    }
    $scope.dateOptions = {
        formatYear: 'MM/yyyy',
        maxDate: new Date(2020, 5, 22),
        minMode: 'month'
    }
    $scope.format = "MM/yyyy";
    $scope.dateOptions1 = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22)
    }
    $scope.format1 = "dd-MM-yyyy";
    $scope.popup1 = {
        opened: false
    }
    $scope.popup2 = {
        opened: false
    }

	$scope.student_report_init = function(){
		if($scope.schooladmin_present){
            $scope.from_date_change()
            $scope.to_date_change()
            $scope.dtmax = new Date();
            console.log($scope.dtmax)
            $scope.options = {
                maxDate : $scope.dtmax,
            };
		}else{
		}
    }
    $scope.$on('schooladmin', function(event, args) {
        $scope.from_date_change()
        $scope.to_date_change()
    });
}]);


