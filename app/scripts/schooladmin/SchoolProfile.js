'use strict'

angular.module('Kidz').controller('SchoolProfile', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'httpCalling', 'UserQueryApiCalling', '$filter', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, httpCalling, UserQueryApiCalling, $filter) {

	$scope.particular_school = {}
    $scope.date_date =  new Date();
    
    $scope.get_schooldata = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/school_admin/profile/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.deactivate_loading()
            if (data.result) {
                $scope.particular_school = data.data
                $scope.date_date = new Date($scope.particular_school.valid_till);
                $scope.date_date = $filter('date')($scope.date_date, 'd MMMM yyyy')
            }                        
        })
    }

    $scope.reset_password = function(){
        var send_data = {
            "email" : $scope.schooladmin.email
        }
        console.log(send_data)
        var myDataPromise = httpCalling.apiCalling('POST', '/api/school_admin/forgot_password/', send_data);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            if (data.result) {
                $scope.change_message_to_show("A Password-Reset Email has been sent to you. Please click on the link sent to your mail to change your password.", 1)
                $('#redirectBox').modal('show');
                
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }     
        })
    }

	$scope.edit_school_init = function(){
        // console.debug("edit_school_init")
		if($scope.schooladmin_present){
			$scope.get_schooldata()
		}else{
		}
    }

    $scope.$on('schooladmin', function(event, args) {
		$scope.get_schooldata()
    });

}]);


