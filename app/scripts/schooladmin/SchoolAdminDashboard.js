'use strict'

angular.module('Kidz').controller('SchoolAdminDashboard', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserQueryApiCalling', 'UserApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserQueryApiCalling, UserApiCalling) {

    $scope.page_count = 1
    $scope.max_number_of_pages = 1
	$scope.array_of_classes = []
	$scope.selected_class = null
	$scope.array_of_teachers=[]
    $scope.array_of_activities=[]
    $scope.feed_for_approval_id = null
    $scope.particular_feed = {}
    $scope.feed_to_delete={}
    $scope.dashboard_activities={}
    $scope.daily_activity_feed=[]
    $rootScope.state_name_navbar = null
    $scope.corresponding_gallery = {}
    $scope.event_state = true
    $scope.corresponding_diary = {}

	$scope.call_classes_list = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/class/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.array_of_classes = data
        })
    }
    $scope.call_teachers_list = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/staff/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.array_of_teachers = data
        })
    }
    $scope.call_dashboard_api = function(){
        $scope.activate_loading()
        $(document).ready(function () {
            $('#dash_1').height(($(window).height() - 255));
            $(window).resize(function () {
                $('#dash_1').height(($(window).height() - 255));
            });
            $('#dash_2').height(($(window).height() - 355));
            $(window).resize(function () {
                $('#dash_2').height(($(window).height() - 355));
            });

        });

        if ($scope.selected_class) {
        	var url_get = "?page="+$scope.page_count+'&class_id='+$scope.selected_class.id
        }else{
        	var url_get = "?page="+$scope.page_count
        }
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/dashboard/' + url_get, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()

            if (data.result) {
                $scope.checked_in_count = data.data.checked_in_count
                $scope.checked_out_count = data.data.checked_out_count
                $scope.dashboard_events = data.data.events
                $scope.dashboard_activities = data.data.pending_activities
                $scope.dashboard_messages=data.data.messages
            }else{

            }
            $scope.array_of_feeds = data.results
        })
    }

    $scope.select_class = function(class_selected){
        if (class_selected == null) {
            $scope.selected_class = null
        }else{
            $scope.selected_class = class_selected
        }
        $scope.call_dashboard_api()
    }


    $scope.$watch(function(){
        return $state.$current.name
    }, function(newVal, oldVal){
        $rootScope.state_name_navbar = newVal
        window.scrollTo(0, 0);
        if ($state.current.name) {
            $scope.call_dashboard_api()
        }
    }) 

	$scope.dashboard_init = function(){
		if($scope.schooladmin_present){
			$scope.call_classes_list()
			$scope.call_teachers_list()
			$scope.call_dashboard_api()
		}else{
		}
    }

    $scope.$on('schooladmin', function(event, args) {
		$scope.call_classes_list()
		$scope.call_teachers_list()
		$scope.call_dashboard_api()
    });













    $scope.picker3 = {
        date: new Date()
    };

    $scope.openCalendar = function(e, picker) {
        $scope[picker].open = true;
    };
    
    $scope.open3 = function() {
        $scope.popup3.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22)
    };
    $scope.format = "dd-MM-yyyy";
    $scope.popup3 = {
        opened: false
    };






















    // Feed APIs - Functionalities
    $scope.set_to_delete = function(id, segment, segment_id){
        console.debug(id)
        $scope.feed_to_delete = id
        $scope.segment_set = segment
        $scope.segment_id_set = segment_id
    }
    $scope.delete_feed = function(){
        $scope.activate_loading()
        console.log($scope.feed_to_delete)
        var myDataPromise = UserQueryApiCalling.apiCalling('DELETE', '/api/activity/'+$scope.feed_to_delete+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading() 
            if (data.result) {
                $scope.change_message_to_show(data.message, 1)
                $scope.$broadcast("delete_feed");
                $('#redirectBox').modal('show');
                $scope.call_dashboard_api()
                $scope.daily_activity_call($scope.segment_set, $scope.segment_id_set)
                $scope.diary_call($scope.segment_set, $scope.segment_id_set)
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
            
        })
    }

    $scope.set_for_approval = function(idd){
        $scope.feed_for_approval_id = idd
        $scope.segment_set = segment
        $scope.segment_id_set = segment_id
    }
    $scope.approve_activity = function(value){
        if (value==1) {
            var myDataPromise = UserQueryApiCalling.apiCalling('POST', '/api/activity/'+$scope.feed_for_approval_id+'/approve/', $scope.schooladmin.access_token);
            myDataPromise.then(function(result) {  
                var data = result.data;
                if (data.result) {
                    $scope.change_message_to_show("Activity has been successfully approved", 1)
                    $('#redirectBox').modal('show');
                    $scope.call_dashboard_api() 
                }else{
                    $scope.change_message_to_show(data.errors, 2)
                    $('#redirectBox').modal('show');
                }
            })
        }
    }

    $scope.set_feed = function(activity, segment, segment_id){
        console.log(activity, segment, segment_id)
        $scope.particular_feed = activity
        $scope.particular_feed.time2_edit_local = new Date ($scope.particular_feed.time2)
        $scope.particular_feed.time2_edit = new Date ($scope.particular_feed.time2).getTimezoneOffset()
        $scope.particular_feed.time_to_show = new Date( $scope.particular_feed.time2_edit_local.getTime() + ($scope.particular_feed.time2_edit * 60000));
        console.log($scope.particular_feed)

        $scope.segment_set = segment
        $scope.segment_id_set = segment_id
    }

    $scope.edit_feed = function(){
        var send_data = {
            "text":$scope.particular_feed.item_content,
            "time":$scope.particular_feed.time_to_show
        }

        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/activity/'+$scope.particular_feed.id+'/update_details/', send_data, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            if (data.result) {
                $scope.change_message_to_show("The activity has been successfully edited", 1)
                $('#redirectBox').modal('show');
                if ($rootScope.state_name_navbar == 'SchoolAdminDashboard') {
                    $scope.call_dashboard_api() 
                }
                $scope.daily_activity_call($scope.segment_set, $scope.segment_id_set)
                $scope.diary_call($scope.segment_set, $scope.segment_id_set)
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }





























    $scope.gallery_call = function(segment, segment_id){
        if ($scope.segment_function_to_call != "gallery") {
            $scope.segment_function_to_call = "gallery"
            $scope.page_count = 1
        }
        if (segment == "class") {
            var url_get = "?page="+$scope.page_count+'&class_id='+segment_id
        }else if (segment == "teacher"){
            var url_get = "?page="+$scope.page_count+'&teacher_id='+segment_id
        }else if (segment == "student"){
            var url_get = "?page="+$scope.page_count+'&student_id='+segment_id
        }
        console.log(url_get)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/dashboard/gallery/'+url_get, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)
            if (data.result) {
                $scope.corresponding_gallery = data.data

            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    $scope.diary_call = function(segment, segment_id){
        if ($scope.segment_function_to_call != "diary") {
            $scope.segment_function_to_call = "diary"
            $scope.page_count = 1
        }
        var url_get = "?page="+$scope.page_count+'&student_id='+segment_id
        console.log(url_get)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/dashboard/diary/'+url_get, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)
            if (data.result) {
                $scope.corresponding_diary = data.data
                console.log($scope.corresponding_diary)
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    $scope.daily_activity_call = function(segment, segment_id){
        if ($scope.segment_function_to_call != "daily_activity") {
            $scope.segment_function_to_call = "daily_activity"
            $scope.page_count = 1
        }
        if (segment == "class") {
            var url_get = "?page=" + $scope.page_count + '&class_id=' + segment_id
        }else if (segment == "teacher"){
            var url_get = "?page=" + $scope.page_count + '&teacher_id=' + segment_id
        }else if (segment == "student"){
            var url_get = "?page=" + $scope.page_count + '&student_id=' + segment_id
        }
        console.log(url_get)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/dashboard/daily_activities/'+url_get, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)
            if (data.result) {
                $scope.daily_activity_feed=data
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    $scope.messages_call = function(segment, segment_id){
        if ($scope.segment_function_to_call != "messages") {
            $scope.segment_function_to_call = "messages"
            $scope.page_count = 1
        }
        if (segment == "class") {
            var url_get = "?page=" + $scope.page_count + '&class_id=' + segment_id
        }else if (segment == "student"){
            var url_get = "?page=" + $scope.page_count + '&student_id=' + segment_id
        }
        console.log(url_get)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/dashboard/messages/'+url_get, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)
            if (data.result) {
                $scope.corresponding_messages = data.data
                $scope.pagination_function(data)
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    $scope.upcoming_events_call = function(segment, segment_id){
        if ($scope.segment_function_to_call != "upcoming") {
            $scope.segment_function_to_call = "upcoming"
            $scope.page_count = 1
        }
        if (segment == "class") {
            var url_get = "?page=" + $scope.page_count + '&class_id=' + segment_id
        }else if (segment == "teacher"){
            var url_get = "?page=" + $scope.page_count + '&teacher_id=' + segment_id
        }
        console.log(url_get)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/dashboard/upcoming_events/'+url_get, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)
            if (data.result) {
                $scope.corresponding_events = data.data
                $scope.pagination_function(data)
                
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    $scope.past_events_call = function(segment, segment_id){
        if ($scope.segment_function_to_call != "past") {
            $scope.segment_function_to_call = "past"
            $scope.page_count = 1
        }
        if (segment == "class") {
            var url_get = "?page=" + $scope.page_count + '&class_id=' + segment_id
        }else if (segment == "teacher"){
            var url_get = "?page=" + $scope.page_count + '&teacher_id=' + segment_id
        }
        console.log(url_get)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/dashboard/past_events/'+url_get, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)
            if (data.result) {
                $scope.corresponding_events = data.data
                $scope.pagination_function(data)
                $scope.event_state = true
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }






















    $scope.pagination_function = function(data){
        $scope.max_number_of_pages = parseInt(data.count/5)+1
        if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
            $scope.disable_next = false
            if ($scope.page_count>1) {
                $scope.disable_prev = false
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }
        }else if($scope.max_number_of_pages==1){
            $scope.disable_prev = true
            $scope.disable_next = true
        }else if($scope.page_count==1){
            $scope.disable_prev = true
        }else if ($scope.page_count==$scope.max_number_of_pages) {
            $scope.disable_next = true
            $scope.disable_prev = false
        }
    }
    $scope.pagination_count_update = function(segment_function_to_call, count, segment, segment_id){
        console.debug(segment_function_to_call, count, segment, segment_id)
        $scope.segment_function_to_call = segment_function_to_call
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            if (segment_function_to_call=="upcoming") {
                $scope.upcoming_events_call(segment, segment_id)
            }else if (segment_function_to_call=="past") {
                $scope.past_events_call(segment, segment_id)
            }else if (segment_function_to_call == "messages") {
                $scope.messages_call(segment, segment_id)
            }else if (segment_function_to_call == "daily_activity") {
                $scope.daily_activity_call(segment, segment_id)
            }else if (segment_function_to_call == "diary") {
                $scope.diary_call(segment, segment_id)
            }else if (segment_function_to_call == "gallery") {
                $scope.gallery_call(segment, segment_id)
            }
        }
    }

    $scope.page_count_change = function(number, segment_function_to_call, segment, segment_id){
        console.debug(number, segment_function_to_call, segment, segment_id)
        $scope.page_count = number
        if (segment_function_to_call=="upcoming") {
            $scope.upcoming_events_call(segment, segment_id)
        }else if (segment_function_to_call=="past") {
            $scope.past_events_call(segment, segment_id)
        }else if (segment_function_to_call == "messages") {
            $scope.messages_call(segment, segment_id)
        }else if (segment_function_to_call == "daily_activity") {
            $scope.daily_activity_call(segment, segment_id)
        }else if (segment_function_to_call == "diary") {
            $scope.diary_call(segment, segment_id)
        }else if (segment_function_to_call == "gallery") {
            $scope.gallery_call(segment, segment_id)
        }
    }

    $scope.getNumber = function(num) {
        return new Array(num);   
    }

}]);


