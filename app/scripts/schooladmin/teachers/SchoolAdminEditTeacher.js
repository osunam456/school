'use strict'

angular.module('Kidz').controller('SchoolAdminEditTeacher', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling) {

	$scope.particular_teacher = {}
    $scope.particular_teacher.email = ""
    $scope.moderated_activities_form = ''
    $scope.edit_state = false
    $scope.static_activity_names = []
    $scope.all_classes = []

    $scope.particular_teacher.moderated_activities_form = []
    $scope.particular_teacher.classrooms_form = []

    $scope.update_teacher = function(){
        var send_data = {}
        var selected_activities_ids = []
        var moderated_activities = []
        // console.log($scope.particular_teacher.classrooms)
        if ($scope.particular_teacher.moderated_activities_form) {
            moderated_activities = $scope.particular_teacher.moderated_activities_form.split(',')
            console.debug(moderated_activities)
            for (var i = 0; i < moderated_activities.length; i++) {
                $scope.static_activities.forEach(function(activity) {
                    if (moderated_activities[i] == activity.name) {
                        selected_activities_ids.push(activity.id)
                    }
                });
                $scope.learning_activities.forEach(function(activity) {
                    if (moderated_activities[i] == activity.name) {
                        selected_activities_ids.push(activity.id)
                    }
                });
            }
        }
        if ($scope.particular_teacher.classrooms_form) {
            var assigned_classrooms = $scope.particular_teacher.classrooms_form.split(',')
            var selected_classrooms_ids = []

            $scope.all_classes.forEach(function(particular_class) {
                for (var i = 0; i < assigned_classrooms.length; i++) {
                    if (particular_class.name==assigned_classrooms[i]) {
                        selected_classrooms_ids.push(particular_class.id)
                    }
                }
            });

            var selected_classrooms_ids_strings = selected_classrooms_ids.join(",");
            send_data = {
                "name" : $scope.particular_teacher.name,
                "email" : $scope.particular_teacher.email,
                "mobile" : $scope.particular_teacher.mobile,
                "staff_type" : $scope.particular_teacher.staff_type,
                "classes" : selected_classrooms_ids_strings,
                "activities" : selected_activities_ids.join(",")
            }
        }else{
            send_data = {
                "name" : $scope.particular_teacher.name,
                "email" : $scope.particular_teacher.email,
                "mobile" : $scope.particular_teacher.mobile,
                "staff_type" : $scope.particular_teacher.staff_type,
                "activities" : selected_activities_ids.join(",")
            }
        }
        // console.log(send_data)
        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/staff/'+$stateParams.id_teacher+'/update_details/', send_data, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            $scope.deactivate_loading()
            if (data.result) {
                $scope.particular_teacher = data.data

                $scope.change_message_to_show("Teacher has been successfully edited", 1)
                $scope.get_teacherdata ()
                $('#redirectBox').modal('show');
                $location.path("/schooladmin/main/teachers/");
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }     
        })

    }




    $scope.hide_moderated =function(){
        $(".moderated_activites_div").css('display','none')
    }

    $scope.show_moderated =function(){
        $(".moderated_activites_div").css('display','block')
    }



    
    $scope.activate_tags = function(){
        setTimeout(function(){ 
            $scope.static_activities.forEach(function(activity) {
                $scope.static_activity_names.push(activity.name)
            });
            $scope.learning_activities.forEach(function(activity) {
                $scope.static_activity_names.push(activity.name)
            });
            console.log($scope.static_activity_names)
            $("#moderatedActivities").tagit({
                availableTags: $scope.static_activity_names,
                autocomplete: {delay: 0, minLength: 0},
                showAutocompleteOnFocus : true
            });
        }, 500);
    }

    


    $scope.get_teacherdata = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/staff/'+$stateParams.id_teacher+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            $scope.deactivate_loading()
            var data = result.data;
            console.log("get_teacherdata", data)
            if (data.result) {
                $scope.particular_teacher = data.data
                if ($scope.particular_teacher.staff_type=="unmoderated") {
                    $(".moderated_activites_div").css('display','none')
                }
            }
        })
    }

    $scope.activate_classes = function(){
        setTimeout(function(){
            var class_names = []
            $scope.all_classes.forEach(function(particular_class){
                class_names.push(particular_class.name)
            })
            $("#myClasses").tagit({
                availableTags: class_names,
                autocomplete: {delay: 0, minLength: 0},
                showAutocompleteOnFocus : true
            });
        }, 500)
    }

    $scope.create_tags = function(){
        $scope.activate_tags()
        $scope.activate_classes()

        setTimeout(function(){
            var moderated_activities = []
            if ($scope.particular_teacher.moderated_activities) {
                $scope.particular_teacher.moderated_activities.forEach(function(activity){
                    moderated_activities.push(activity.name)
                })
                console.debug("activities")
                // $("#moderatedActivities").tagit("removeAll");
                moderated_activities.forEach(function(activity){
                    console.log(activity)
                    $("#moderatedActivities").tagit("createTag", activity);
                })  
            }

            var classes_selected = []
            $scope.particular_teacher.classes.forEach(function(classroom){
                classes_selected.push(classroom)
            })
            classes_selected.forEach(function(activity){
                $("#myClasses").tagit("createTag", activity.name);
            })
        }, 500)
    }

    $scope.get_all_classes = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/class/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.all_classes = data     
        })
    }

    $scope.trigger_edit = function(){
        $scope.edit_state = true
        $scope.create_tags()
    }

    $scope.cancel_edit = function(){
        $scope.edit_state = false
    }

	$scope.edit_teachers_init = function(){
		if($scope.schooladmin_present){
            $scope.get_all_classes()
            $scope.activate_classes()
            $scope.activate_tags()
            $scope.get_teacherdata()
		}else{

		}
    }

    $scope.$on('schooladmin', function(event, args) {
        $scope.get_all_classes()
        $scope.activate_classes()
        $scope.activate_tags()
        $scope.get_teacherdata()
    });
}]);


