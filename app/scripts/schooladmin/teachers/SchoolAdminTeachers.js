'use strict'

angular.module('Kidz').controller('SchoolAdminTeachers', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling) {

	$scope.page_count = 1
    $rootScope.thing_to_search = ""
    $scope.search = ""
    $rootScope.array_of_teachers = []
    $scope.view = false

    $scope.teachers_list = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/staff/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            $rootScope.array_of_teachers = []
            $rootScope.array_of_teachers = data.results
            $scope.total_teacher_count = data.length
            console.log(data)
        })
    }


    $scope.search_function = function(){
        $rootScope.thing_to_search = $scope.search
        $scope.teachers_list()
    }

    $scope.edit_teachers_page = function(id){
        $location.path("/schooladmin/main/edit-teacher/"+id+"/")    
    }

    $scope.set_to_delete = function(id){
        $scope.teacher_to_delete = id
    }
    $scope.delete_teacher_page = function(id){
        console.log($scope.teacher_to_delete)
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('DELETE', '/api/staff/'+$scope.teacher_to_delete+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            $scope.deactivate_loading() 
            if (data.result) {
                $scope.change_message_to_show(data.message, 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
            $scope.teachers_list()
        })
    }

    $scope.pagination_count_update = function(count, segment_function){
        // console.debug(count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope[segment_function]()
        }
    }
    
    $scope.page_count_change = function(number, segment_function){
        // console.debug($scope.page_count)
        $scope.page_count = number
        $scope[segment_function]()
    }

    $scope.getNumber = function(num) {
        return new Array(num);   
    }


	$scope.teachers_init = function(){
		if($scope.schooladmin_present){
			$scope.teachers_list()
		}else{
		}
    }

    $scope.$on('schooladmin', function(event, args) {
		$scope.teachers_list()
    });
}]);


