'use strict'

angular.module('Kidz').controller('SchoolAdminCreateTeacher', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling) {

    $scope.new_teacher = {}
    $scope.new_teacher.email = ""

    $scope.static_activity_names = []

    $scope.activate_tags = function(){
        if ($scope.static_activities) {
            $scope.static_activities.forEach(function(activity) {
                $scope.static_activity_names.push(activity.name)
            });
            $scope.learning_activities.forEach(function(activity) {
                $scope.static_activity_names.push(activity.name)
            }); 
            setTimeout(function(){ 
                $("#moderatedActivities").tagit({
                    availableTags: $scope.static_activity_names,
                    autocomplete: {delay: 0, minLength: 0},
                    showAutocompleteOnFocus : true
                });
                $scope.static_activity_names.forEach(function(activity){
                    $("#moderatedActivities").tagit("createTag", activity);
                })  
            }, 500);
        }
    }

    $scope.get_all_classes = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/class/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.all_classes = data

            var class_names = []
            $scope.all_classes.forEach(function(particular_class){
                class_names.push(particular_class.name)
            })
            setTimeout(function(){ 
                $("#myClasses").tagit({
                    availableTags: class_names,
                    autocomplete: {delay: 0, minLength: 0},
                    showAutocompleteOnFocus : true
                });
            }, 500);
                        
        })
    }

    $scope.create_teacher = function(form){
        form.submitted = true;
        var activities_ids_selected = []
        if (form.$valid) {
            console.log(form)
            if ($scope.new_teacher.moderated_activities) {
                console.log($scope.new_teacher.moderated_activities)
                var default_activities = $scope.new_teacher.moderated_activities.split(',')
                console.log(default_activities)
                $scope.static_activities.forEach(function(activity) {
                    for (var i = 0; i < default_activities.length; i++) {
                        if (activity.name==default_activities[i]) {
                            activities_ids_selected.push(activity.id) 
                        }
                    }
                });
                $scope.learning_activities.forEach(function(activity) {
                    for (var i = 0; i < default_activities.length; i++) {
                        if (activity.name==default_activities[i]) {
                            activities_ids_selected.push(activity.id) 
                        }
                    }
                });
            }
            if ($scope.new_teacher.classrooms) {
                var assigned_classrooms = $scope.new_teacher.classrooms.split(',')
                var selected_classrooms_ids = []
                $scope.all_classes.forEach(function(particular_class) {
                    for (var i = 0; i < assigned_classrooms.length; i++) {
                        if (particular_class.name==assigned_classrooms[i]) {
                            selected_classrooms_ids.push(particular_class.id)
                        }
                    }
                });
                var selected_classrooms_ids_strings = selected_classrooms_ids.join(",");
                var send_data = {
                    "name" : $scope.new_teacher.name,
                    "email" : $scope.new_teacher.email,
                    "mobile" : $scope.new_teacher.mobile,
                    "staff_type" : $scope.new_teacher.type,
                    "classes" : selected_classrooms_ids_strings,
                    "activities": activities_ids_selected
                }
            }else{
                var send_data = {
                    "name" : $scope.new_teacher.name,
                    "email" : $scope.new_teacher.email,
                    "mobile" : $scope.new_teacher.mobile,
                    "staff_type" : $scope.new_teacher.type,
                    "activities": activities_ids_selected
                }
            }
            // console.log(send_data)
            $scope.activate_loading()
            var myDataPromise = UserApiCalling.apiCalling('POST', '/api/staff/', send_data, $scope.schooladmin.access_token);
            myDataPromise.then(function(result) {  
                // console.log(result)
                var data = result.data;
                $scope.deactivate_loading()
                if (data.result) {
                   $location.path("/schooladmin/main/teachers/")
                }else{
                    $scope.change_message_to_show(data.errors, 2)
                    $('#redirectBox').modal('show');
                }
            })
        }
    }

	$scope.create_teacher_init = function(){
		if($scope.schooladmin_present){
            $scope.get_all_classes()
            $scope.activate_tags()
		}else{
		}
    }
    $scope.$on('schooladmin', function(event, args) {
        $scope.get_all_classes()
        $scope.activate_tags()
    });
}]);


