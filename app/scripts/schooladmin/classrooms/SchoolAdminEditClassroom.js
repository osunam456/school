'use strict'

angular.module('Kidz').controller('SchoolAdminEditClassroom', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling) {

	$scope.particular_classroom = {}
    $scope.particular_classroom.id = $stateParams.id_classroom
    $scope.corresponding_class_students = []
    $scope.list_to_show=[]
    $scope.all_students = []
    $scope.activities_in_class = null
    $scope.students_to_remove = []

    $scope.static_activity_names = []
    $scope.learning_activity_names = []

    $scope.particular_classroom_learning_activities = []
    $scope.particular_classroom_default_activities = []

    $scope.particular_classroom.learning_activities_form = {}
    $scope.particular_classroom.default_activities_form = {}

    $scope.edit_state = false

	$scope.activate_tags = function(){
		console.debug("activate_tags", $scope.static_activities)
        if ($scope.static_activities) {
            $scope.static_activities.forEach(function(activity) {
                $scope.static_activity_names.push(activity.name)
            });
            $scope.learning_activities.forEach(function(activity) {
                $scope.learning_activity_names.push(activity.name)
            });
            $("#learningActivities").tagit({
                availableTags: $scope.learning_activity_names,
                autocomplete: {delay: 0, minLength: 0},
                showAutocompleteOnFocus : true
            });
            $("#defaultActivities").tagit({
                availableTags: $scope.static_activity_names,
                autocomplete: {delay: 0, minLength: 0},
                showAutocompleteOnFocus : true
            });
            return true
        }else{
            return false
        }
    }

    $scope.get_classroomdata = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/class/'+$stateParams.id_classroom+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)
            if (data.result) {
                $scope.particular_classroom = data.data
                
                $scope.particular_classroom_learning_activities = []
                $scope.particular_classroom_default_activities = []

                $scope.particular_classroom.activities.forEach(function(activity){
                    if (activity.is_learning) {
                        $scope.particular_classroom_learning_activities.push(activity)
                    }else{
                        $scope.particular_classroom_default_activities.push(activity)
                    }
                })
            }
                       
        })
    }

    $scope.call_students = function(){
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/student/get_all/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            
            $scope.all_students = data
            $scope.list_to_show = $scope.all_students
            
            console.log($scope.all_students)
            var indexes_to_splice = []
            if ($scope.particular_classroom.students) {
                $scope.particular_classroom.students.forEach(function(corresponding_student){
                    $scope.list_to_show.forEach(function(particular_student){
                        if (particular_student.id==corresponding_student.id) {
                            var index = $scope.list_to_show.indexOf(particular_student)
                            indexes_to_splice.push(index)
                            $scope.list_to_show.splice(index, 1)
                        }
                    })
                }) 
            }
        })
    }

    $scope.create_learning_activity = function(){
        console.log($scope.new_learning_activity)
        var send_data = {
            name : $scope.new_learning_activity.name
        }
        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/class/add_learning_activity/', send_data, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            console.log(data)
            $scope.deactivate_loading()
            if (data.result) {
                $scope.learning_activities.push(data.data)
                $scope.learning_activity_names.push(data.data.name)
                $("#learningActivities").tagit({
                    availableTags: $scope.learning_activity_names,
                    autocomplete: {delay: 0, minLength: 0},
                    showAutocompleteOnFocus : true
                });
                $scope.new_learning_activity = {}
                $scope.change_message_to_show(data.message, 1)
                $('#redirectBox').modal('show');
                 
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    

    

    $scope.trigger_edit = function(){
        $scope.edit_state = true
        setTimeout(function(){ 
            console.log($scope.learning_activity_names, $scope.static_activity_names)
            
            $("#learningActivities").tagit({
                availableTags: $scope.learning_activity_names,
                autocomplete: {delay: 0, minLength: 0},
                showAutocompleteOnFocus : true
            });
            $("#defaultActivities").tagit({
                availableTags: $scope.static_activity_names,
                autocomplete: {delay: 0, minLength: 0},
                showAutocompleteOnFocus : true
            });

            $("#learningActivities").tagit("removeAll");
            $("#defaultActivities").tagit("removeAll");
            
            $scope.particular_classroom_learning_activities.forEach(function(activity){
                console.log("particular_classroom_learning_activities", activity.name)
                $("#learningActivities").tagit("createTag", activity.name);
            })   
            
            $scope.particular_classroom_default_activities.forEach(function(activity){
                $("#defaultActivities").tagit("createTag", activity.name);
            })   
        }, 1200) 
    }
    $scope.cancel_edit = function(){
        $scope.edit_state = false
    }







    $scope.add_student = function(id){
        console.log("add_student")
        $('#'+id).css('opacity', '0.3')
        $('#'+id+'_select_check').css('z-index', '1')
        $('#'+id+'_select_check').css('opacity', '1')
        if ($scope.corresponding_class_students.length == 0) {
            $scope.corresponding_class_students.push(id)
        }else{
            var index = $scope.corresponding_class_students.indexOf(id)
            console.log(index)
            if (index == -1) {
                $scope.corresponding_class_students.push(id)
            }else{
                $scope.corresponding_class_students.splice(index, 1)
                $('#'+id).css('color', 'black')
                $('#'+id).css('opacity', '1')
                $('#'+id+'_select_check').css('z-index', '-1')
            }
        }
        console.log($scope.corresponding_class_students)
    }

    $scope.remove_student = function(id){
        $('#'+id+'_r').css('opacity', '0.3')
        $('#'+id+'_select_check_r').css('z-index', '1')
        $('#'+id+'_select_check_r').css('opacity', '1')
        console.log($scope.students_to_remove)
        var index = $scope.students_to_remove.indexOf(id)
        console.log(index)
        if (index == -1) {
            $scope.students_to_remove.push(id)
        }else{
            $scope.students_to_remove.splice(index, 1)
            $('#'+id+'_r').css('color', 'black')
            $('#'+id+'_r').css('opacity', '1')
            $('#'+id+'_select_check_r').css('z-index', '-1')
        }
        console.log($scope.students_to_remove)
    }















    
    $scope.update_classroom = function(parameter_to_send, type){
        console.log(parameter_to_send)
        if (type=="students") {
            // Students
            var students_in_class = []
            if (parameter_to_send=="add") {
                
                $scope.corresponding_class_students.forEach(function(idd){
                    students_in_class.push(idd)
                })
                $scope.particular_classroom.students.forEach(function(student){
                    students_in_class.push(student.id)
                })

                $scope.particular_classroom.students = students_in_class.join(",")
                console.log($scope.particular_classroom.students)
            }else if (parameter_to_send=="remove"){
                $scope.students_to_remove.forEach(function(idd){
                    // students_in_class.push(idd)
                    console.log($scope.particular_classroom.students)
                    $scope.particular_classroom.students.forEach(function(student){
                        if (student.id == idd) {
                            var index = $scope.particular_classroom.students.indexOf(student)
                            $scope.particular_classroom.students.splice(index, 1)
                        }
                    })
                })
                console.log($scope.particular_classroom.students)
                $scope.particular_classroom.students.forEach(function(student){
                    students_in_class.push(student.id)
                })
                $scope.particular_classroom.students = students_in_class.join(",");
            }else{
                console.log($scope.particular_classroom.students)
                $scope.particular_classroom.students.forEach(function(student){
                    students_in_class.push(student.id)
                })
                $scope.particular_classroom.students = students_in_class.join(",");
            }
            console.log($scope.particular_classroom.students)


            //Activites
            var selected_activities_ids = []
            $scope.particular_classroom_learning_activities.forEach(function(activity) {
                selected_activities_ids.push(activity.id)
            });
            $scope.particular_classroom_default_activities.forEach(function(activity) {
                selected_activities_ids.push(activity.id)
            });
            $scope.particular_classroom.activities = selected_activities_ids.join(",");
        }else{
            //Students
            var students_in_class = []
            console.log($scope.particular_classroom.students)
            $scope.particular_classroom.students.forEach(function(student){
                students_in_class.push(student.id)
            })
            $scope.particular_classroom.students = students_in_class.join(",");

            //Activities
            var selected_activities_ids = []
            var learning_activities = []
            var default_activities = []

            console.log($scope.particular_classroom.learning_activities_form, $scope.particular_classroom.default_activities_form)

            if ($scope.particular_classroom.learning_activities_form) {
                learning_activities = $scope.particular_classroom.learning_activities_form.split(',')
                console.debug(learning_activities)
                console.log($scope.particular_classroom.learning_activities_form)
                for (var i = 0; i < learning_activities.length; i++) {
                    $scope.learning_activities.forEach(function(activity) {
                        if (learning_activities[i] == activity.name) {
                            selected_activities_ids.push(activity.id)
                        }
                    });
                }
            }
            if ($scope.particular_classroom.default_activities_form) {
                default_activities = $scope.particular_classroom.default_activities_form.split(',')
                console.debug(default_activities)
                console.log($scope.particular_classroom.default_activities_form)
                for (var i = 0; i < default_activities.length; i++) {
                    $scope.static_activities.forEach(function(activity) {
                        if (default_activities[i] == activity.name) {
                            selected_activities_ids.push(activity.id)
                        }
                    });
                }
            }
            $scope.particular_classroom.activities = selected_activities_ids.join(",");
            console.log($scope.particular_classroom.activities )
        }
        
        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/class/'+$stateParams.id_classroom+'/update_details/', $scope.particular_classroom, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            if (data.result) {
                $scope.particular_classroom = data.data

                $scope.change_message_to_show("Classroom has been successfully edited", 1)
                $('#redirectBox').modal('show');
                $location.path("/schooladmin/main/classrooms/");
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }       
        })
    }	





	$scope.edit_classroom_init = function(){
		if($scope.schooladmin_present){
			var activated = $scope.activate_tags()
            if (activated) {
                $scope.get_classroomdata()
                $scope.call_students()
            }
		}
    }

    $scope.$on('activities', function(event, args) {
        var activated = $scope.activate_tags()
        if (activated) {
            $scope.get_classroomdata()
            $scope.call_students()
        }
    });

    $scope.$on('schooladmin', function(event, args) {
        var activated = $scope.activate_tags()
        if (activated) {
            $scope.get_classroomdata()
            $scope.call_students()
        }
    });

    $scope.$on('delete_feed', function(event, args) {
        $scope.get_classroomdata()
    });
}]);


