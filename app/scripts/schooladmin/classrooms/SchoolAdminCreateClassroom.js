'use strict'


angular.module('Kidz').controller('SchoolAdminCreateClassroom', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling) {

    $scope.new_classroom = {}
    $scope.new_learning_activity = {}

    $scope.static_activity_names = []
    $scope.learning_activities_names = []

    $scope.cancel_function = function(){
        $scope.new_learning_activity = {}
        $scope.new_classroom = {}
    }

    $scope.activate_tags = function(){
        if ($scope.static_activities) {
            console.log("sdsd")
            $scope.static_activities.forEach(function(activity) {
                $scope.static_activity_names.push(activity.name)
            });
            $scope.learning_activities.forEach(function(activity) {
                $scope.learning_activities_names.push(activity.name)
            });
            setTimeout(function(){ 
                $("#defaultActivities").tagit({
                    availableTags: $scope.static_activity_names,
                    autocomplete: {delay: 0, minLength: 0},
                    showAutocompleteOnFocus : true
                });
                $("#learningActivities").tagit({
                    availableTags: $scope.learning_activities_names,
                    autocomplete: {delay: 0, minLength: 0},
                    showAutocompleteOnFocus : true
                });

                var default_activities_array = []
                var non_default_activities_array = []
                $scope.static_activities.forEach(function(activity){
                    if (activity.is_default) {
                        $("#defaultActivities").tagit("createTag", activity.name);
                    }else{
                        non_default_activities_array.push(activity)
                    }
                    console.log("tagit")
                    
                    // $("#defaultActivities").tagit({
                    //     availableTags: $scope.static_activity_names,
                    //     autocomplete: {delay: 0, minLength: 0},
                    //     showAutocompleteOnFocus : true
                    // });
                    
                })   
            }, 1200);
            // $scope.new_classroom.activities = default_activities_array.join(",");
            console.log($scope.new_classroom)
        }
    }

    $scope.create_learning_activity = function(){
        console.log($scope.new_learning_activity)
        // if (!$scope.new_learning_activity.is_diary) {
        //     $scope.new_learning_activity.is_diary = false
        // }
        var send_data = {
            name : $scope.new_learning_activity.name
            // is_diary : $scope.new_learning_activity.is_diary
        }
        $scope.activate_loading()
        var myDataPromise = UserApiCalling.apiCalling('POST', '/api/class/add_learning_activity/', $scope.new_learning_activity, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            console.log(data)
            $scope.deactivate_loading()
            if (data.result) {
                $scope.learning_activities_names.push(data.data.name)
                $scope.learning_activities.push(data.data)
                console.log($scope.learning_activities_names)
                $("#learningActivities").tagit({
                    availableTags: $scope.learning_activities_names,
                    autocomplete: {delay: 0, minLength: 0},
                    showAutocompleteOnFocus : true
                });
                $scope.new_learning_activity = {}
                $scope.change_message_to_show(data.message, 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
        })
    }

    $scope.create_classroom = function(form){
        form.submitted = true;
        console.log("valid")
        if (form.$valid) {
            console.log("valid")

            var name = $scope.new_classroom.name
            console.log(name)
            var activities_ids_selected = []
            if ($scope.new_classroom.learning_activities) {
                var learning_activities = $scope.new_classroom.learning_activities.split(',')
                console.log(learning_activities)
                $scope.learning_activities.forEach(function(activity) {
                    for (var i = 0; i < learning_activities.length; i++) {
                        if (activity.name==learning_activities[i]) {
                            activities_ids_selected.push(activity.id) 
                        }
                    }
                });
            }
            console.log($scope.new_classroom)
            if ($scope.new_classroom.default_activities) {
                console.log($scope.new_classroom.default_activities)
                var default_activities = $scope.new_classroom.default_activities.split(',')
                console.log(default_activities)
                $scope.static_activities.forEach(function(activity) {
                    for (var i = 0; i < default_activities.length; i++) {
                        if (activity.name==default_activities[i]) {
                            activities_ids_selected.push(activity.id) 
                        }
                    }
                    
                });
            }
            
            var selected_activities_ids_string = activities_ids_selected.join(',')
            console.log(selected_activities_ids_string)
            var send_data = {
                "name" : $scope.new_classroom.name,
                "activities" : selected_activities_ids_string
            }
            console.log(send_data)
            $scope.activate_loading()
            var myDataPromise = UserApiCalling.apiCalling('POST', '/api/class/', send_data, $scope.schooladmin.access_token);
            myDataPromise.then(function(result) {  
                var data = result.data;
                $scope.deactivate_loading()
                if (data.result) {
                    $location.path("/schooladmin/main/classrooms/")
                }else{
                    $scope.change_message_to_show(data.errors, 2)
                    $('#redirectBox').modal('show');
                }
            })
        }
    }

    $scope.create_classroom_init = function(){
        if($scope.schooladmin_present){
            $scope.activate_tags()
        }else{
        }
    }

    $scope.$on('schooladmin', function(event, args) {
        $scope.activate_tags()
    });

    $scope.$on('activities', function(event, args) {
        $scope.activate_tags()
    });

}]);


