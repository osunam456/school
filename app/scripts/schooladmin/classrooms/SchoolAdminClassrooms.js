'use strict'

angular.module('Kidz').controller('SchoolAdminClassrooms', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling) {

	$scope.page_count = 1
    $scope.thing_to_search = ""
    $scope.search = ""
    $rootScope.array_of_classrooms = []
    $scope.total_classes_count = 1
    $scope.view = false

    $scope.classrooms_list = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/class/?q='+$scope.thing_to_search+'&page='+$scope.page_count, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            console.log(result)
            $scope.deactivate_loading()
            var data = result.data;

            $scope.max_number_of_pages = parseInt(data.count/10)+1
            if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                $scope.disable_next = false
                if ($scope.page_count>1) {
                    $scope.disable_prev = false
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }
            }else if($scope.max_number_of_pages==1){
                $scope.disable_prev = true
                $scope.disable_next = true
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }else if ($scope.page_count==$scope.max_number_of_pages) {
                $scope.disable_next = true
                $scope.disable_prev = false
            }

            $rootScope.array_of_classrooms = data.results
            $scope.total_classes_count = data.count
        })
    }

    $scope.edit_classroom_page = function(id){
        $location.path("/schooladmin/main/edit-classroom/"+id+"/")    
    }

    $scope.set_to_delete = function(id){
        $scope.class_to_delete = id
        console.log("log")
    }

    $scope.delete_classroom_page = function(id){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('DELETE', '/api/class/'+$scope.class_to_delete+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            $scope.deactivate_loading() 
            if (data.result) {
                $scope.change_message_to_show(data.message, 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
            $scope.classrooms_list()
        })
    }

    $scope.search_function = function(){
        $scope.thing_to_search = $scope.search
        $scope.classrooms_list()
    }


    $scope.pagination_count_update = function(count, segment_function){
        // console.debug(count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope[segment_function]()
        }
    }

    $scope.page_count_change = function(number, segment_function){
        $scope.page_count = number
        $scope[segment_function]()
    }

	$scope.classrooms_init = function(){
		if($scope.schooladmin_present){
			$scope.classrooms_list($scope.page_count)
		}else{
		}
    }

    $scope.$on('schooladmin', function(event, args) {
		$scope.classrooms_list($scope.page_count)
    });
}]);


