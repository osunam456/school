'use strict'


angular.module('Kidz').controller('SchoolAdminEditEssential', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', 'uploadImage', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling, uploadImage) {

	$scope.particular_essential = {}
    $scope.image_change = 0
    $scope.particular_image = null
    $scope.edit_state = false

    $scope.$watch('particular_image', function() {
        // console.debug('particular_image')
        if ($scope.particular_image==null) {
            $scope.image_change = 0
        }else{
            $scope.particular_essential.image = $scope.particular_image
            $scope.image_change = 1
        }
    }, true);

    $scope.update_essential = function(){
        // console.debug($scope.particular_essential)
        if ($scope.image_change==0) {
            delete $scope.particular_essential.image
        }
        $scope.activate_loading()
        var myDataPromise = uploadImage.apiCalling('POST', '/api/essential/'+$stateParams.id_essential+'/update_details/', $scope.particular_essential, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            // console.log(data.result)
            $scope.deactivate_loading()
            if (data.result) {
                $scope.particular_essential = data.data
                $location.path("/schooladmin/main/essentials/");
                $scope.change_message_to_show("Essential has been updated", 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }                        
        })
    }

    $scope.get_essentialdata = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/essential/'+$stateParams.id_essential+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            // console.log(data)
            $scope.deactivate_loading()
            if (data.result) {
                $scope.particular_essential = data.data
            }                        
        })
    }

    $scope.trigger_edit = function(){
        $scope.edit_state = true
    }

    $scope.cancel_edit = function(){
        $scope.edit_state = false
    }

	$scope.edit_essential_init = function(){
		if($scope.schooladmin_present){
			$scope.get_essentialdata()
		}else{
		}
    }


    $scope.$on('schooladmin', function(event, args) {
		$scope.get_essentialdata()
    });

}]);


