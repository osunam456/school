'use strict'

angular.module('Kidz').controller('SchoolAdminEssentials', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling) {

	$scope.page_count = 1
	$scope.ownership_status = "all"
    $rootScope.thing_to_search = ""
    $scope.search = ""
    $rootScope.array_of_essentials = []
    $scope.view = false
    $scope.essential_to_delete = 1
    
    $scope.call_essentials_list = function(){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('GET', '/api/essential/?q='+$rootScope.thing_to_search+'&page='+$scope.page_count+'&filter='+$scope.ownership_status, $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()

            $scope.max_number_of_pages = parseInt(data.count/10)+1
            if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                $scope.disable_next = false
                if ($scope.page_count>1) {
                    $scope.disable_prev = false
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }
            }else if($scope.max_number_of_pages==1){
                $scope.disable_prev = true
                $scope.disable_next = true
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }else if ($scope.page_count==$scope.max_number_of_pages) {
                $scope.disable_next = true
                $scope.disable_prev = false
            }

            $rootScope.array_of_essentials = data.results
            $scope.total_essential_count = data.count
        })
    }

    $scope.search_function = function(){
        $rootScope.thing_to_search = $scope.search
        $scope.call_essentials_list()
    }

    $scope.edit_essential_page = function(id){
        $location.path("/schooladmin/main/edit-essential/"+id+"/")    
    }

    $scope.set_to_delete = function(id){
        $scope.essential_to_delete = id
        
    }

    $scope.delete_essential_page = function(id){
        $scope.activate_loading()
        var myDataPromise = UserQueryApiCalling.apiCalling('DELETE', '/api/essential/'+$scope.essential_to_delete+'/', $scope.schooladmin.access_token);
        myDataPromise.then(function(result) {  
            // console.log(result)
            var data = result.data;
            $scope.deactivate_loading() 
            if (data.result) {
                $scope.change_message_to_show(data.message, 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.change_message_to_show(data.errors, 2)
                $('#redirectBox').modal('show');
            }
            $scope.call_essentials_list()
        })
    }
    $scope.pagination_count_update = function(count, segment_function){
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope[segment_function]()
        }
    }
    
    $scope.page_count_change = function(number, segment_function){
        $scope.page_count = number
        $scope[segment_function]()
    }
    $scope.change_ownership = function(value){
        $scope.ownership_status=value
        $scope.call_essentials_list()
    }


	$scope.essentials_init = function(){
		if($scope.schooladmin_present){
			$scope.call_essentials_list()
		}else{
		}
    }
    $scope.$on('schooladmin', function(event, args) {
		$scope.call_essentials_list()
    });
}]);


