'use strict'

angular.module('Kidz').controller('SchoolAdminCreateEssential', ['$scope', '$state', '$http', "$location", "$stateParams", "$window", "$rootScope", '$log', 'UserApiCalling', 'UserQueryApiCalling', '$httpParamSerializer', '$httpParamSerializerJQLike', 'uploadImage', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, UserQueryApiCalling, $httpParamSerializer, $httpParamSerializerJQLike, uploadImage ) {

	$scope.new_essential = {}

    $scope.create_essential = function(form){
    	form.submitted = true;
        if (form.$valid) {
            if (!$scope.new_essential.image) {
                $scope.change_message_to_show("Please upload an image", 1)
                $('#redirectBox').modal('show');
            }else{
                $scope.activate_loading()
                var myDataPromise = uploadImage.apiCalling('POST', '/api/essential/', $scope.new_essential, $scope.schooladmin.access_token);
                myDataPromise.then(function(result) { 
                	// console.log(result) 
                    var data = result.data;
                    $scope.deactivate_loading()
                    if (data.result) {
                       $location.path("/schooladmin/main/essentials/")
                    }else{
                        $scope.change_message_to_show(data.errors, 2)
                        $('#redirectBox').modal('show');
                    }
                })
            }
        }
    }

	$scope.create_essential_init = function(){
		if($scope.schooladmin_present){

		}else{
		}
    }

    $scope.$on('schooladmin', function(event, args) {
    });

}]);


